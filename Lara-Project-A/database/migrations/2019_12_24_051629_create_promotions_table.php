<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePromotionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('promotions', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('code');
            $table->enum('sign_off_type', ['fixed_amount', 'percentage']);
            $table->float('amount');
            $table->longText('description', 255)->nullable();
            $table->longText('vendors', 255)->nullable()->collation('utf8mb4_bin');
            $table->string('limit_per_coupon')->nullable();
            $table->longText('services', 255)->nullable()->collation('utf8mb4_bin');
            $table->string('limit_per_customer')->nullable();
            $table->date('start_date');
            $table->date('end_date');
            $table->tinyInteger('status')->default('1')->comment('1 => Active, 0 => Inactive');
            $table->tinyInteger('is_deleted')->default('0')->comment('1 => Yes, 0 => No');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('promotions');
    }
}
