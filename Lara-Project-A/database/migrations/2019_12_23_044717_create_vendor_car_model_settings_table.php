<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateVendorCarModelSettingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('vendor_car_model_settings', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('vendor_id')->index();
            $table->integer('car_model_id')->index();
            $table->enum('segment', ['small', 'medium', 'large', 'premium']);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_car_model_segments');
    }
}
