<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUserAddressesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_addresses', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id')->index();
            $table->string('address_line_1');
            $table->string('address_line_2')->nullable();
            $table->integer('state_id')->index();
            $table->integer('city_id')->index();
            $table->string('latitude');
            $table->string('longitude');
            $table->string('zipcode');
            $table->tinyInteger('is_primary')->default('0')->comment('1 => Yes, 0 => No');
            $table->tinyInteger('is_deleted')->default('0')->comment('1 => Yes, 0 => No');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_addresses');
    }
}
