<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orders', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('temp_order_id')->index()->nullable();
            $table->integer('customer_id')->index();
            $table->integer('vendor_id')->index();
            $table->integer('user_car_id')->index();
            $table->integer('user_address_id')->index()->nullable();
            $table->string('sequence_id');
            $table->enum('pickup_type', ['self_drop', 'pickup']);
            $table->integer('pickup_person_id')->index()->nullable();
            $table->float('sub_total');
            $table->float('tax');
            $table->float('total');
            $table->dateTime('service_date');
            $table->enum('status', ['order_received','order_confirmed','order_cancelled','team_enroute','car_picked_up','service_started','service_completed','delivered']);
            $table->string('reason_for_cancellation')->nullable();
            $table->tinyInteger('is_awaiting_for_approval')->default('0')->comment('1 => Yes, 0 => No');
            $table->tinyInteger('is_deleted')->default('0')->comment('1 => Yes, 0 => No');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('orders');
    }
}
