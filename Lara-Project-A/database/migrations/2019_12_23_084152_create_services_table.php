<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateServicesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('services', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('parent_id')->default(0)->index();
            $table->string('name')->unique();
            $table->longText('description', 255)->nullable();
            $table->string('image')->nullable();
            $table->tinyInteger('status')->default('1')->comment('1 => Active, 0 => Inactive');
            $table->tinyInteger('is_deleted')->default('0')->comment('1 => Yes, 0 => No');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('services');
    }
}
