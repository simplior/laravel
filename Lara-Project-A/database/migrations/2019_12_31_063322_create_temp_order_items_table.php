<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTempOrderItemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('temp_order_items', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('temp_order_id')->index();
            $table->integer('parent_service_id')->index()->nullable();
            $table->integer('service_id')->index()->nullable();
            $table->string('service_name');
            $table->enum('cost_type', ['service_cost', 'part_cost'])->default('service_cost');
            $table->float('cost');
            $table->float('tax')->comment('tax in %');
            $table->float('total');
            $table->enum('status', ['waiting_for_confirmation', 'confirmed', 'cancelled']);
            $table->tinyInteger('is_custom')->default('0')->comment('1 => Yes, 0 => No');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('temp_order_items');
    }
}
