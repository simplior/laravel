<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateLogsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('logs', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('common_id')->index();
            $table->enum('action_type', ['login', 'order', 'vendor', 'promotion']);
            $table->enum('action', ['add','edit','delete','change_status','logged_in','logged_out','forgot_password','password_changed']);
            $table->string('old')->nullable();
            $table->string('new')->nullable();
            $table->longText('description')->nullable();
            $table->integer('action_by')->index();
            $table->string('user_ip');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('logs');
    }
}
