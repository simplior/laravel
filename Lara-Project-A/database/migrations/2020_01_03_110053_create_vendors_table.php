<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateVendorsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('vendors', function (Blueprint $table) {
            $table->increments('id');
            $table->string('sequence_id');
            $table->string('code');
            $table->string('name');
            $table->string('contact_no');
            $table->string('email')->nullable();
            $table->string('address_line_1');
            $table->string('address_line_2')->nullable();
            $table->integer('state_id')->index();
            $table->integer('city_id')->index();
            $table->string('latitude')->nullable();
            $table->string('longitude')->nullable();
            $table->string('zipcode');
            $table->string('logo')->nullable();
            $table->string('GST_number')->nullable();
            $table->string('GST_certificate')->nullable();
            $table->string('agreement_number')->nullable();
            $table->string('agreement')->nullable();
            $table->string('emergency_contact_no')->nullable();
            $table->tinyInteger('status')->default('1')->comment('1 => Active, 0 => Inactive');
            $table->tinyInteger('is_deleted')->default('0')->comment('1 => Yes, 0 => No');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('vendors');
    }
}
