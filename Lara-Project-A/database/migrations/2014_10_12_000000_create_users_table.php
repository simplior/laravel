<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('vendor_id')->nullable();
            $table->enum('role', ['admin', 'customer', 'support', 'vendor_admin', 'manager', 'pickup_person']);
            $table->string('sequence_id')->nullable();
            $table->string('code')->nullable();
            $table->string('name');
            $table->string('email')->unique()->nullable();
            $table->string('contact_no')->nullable();
            $table->timestamp('email_verified_at')->nullable();
            $table->string('password');
            $table->rememberToken();
            $table->string('profile_pic')->nullable();
            $table->string('aadhar_card_number')->nullable();
            $table->string('PAN_number')->nullable();
            $table->string('aadhar_card')->nullable();
            $table->string('PAN_card')->nullable();
            $table->enum('registered_via', ['portal', 'application', 'google', 'facebook']);
            $table->tinyInteger('is_name_verified')->default('1')->comment('1 => Yes, 0 => No');
            $table->tinyInteger('is_contact_no_verified')->default('1')->comment('1 => Yes, 0 => No');
            $table->tinyInteger('is_email_verified')->default('0')->comment('1 => Yes, 0 => No');
            $table->tinyInteger('status')->default('1')->comment('1 => Active, 0 => Inactive');
            $table->tinyInteger('is_deleted')->default('0')->comment('1 => Yes, 0 => No');
            $table->timestamps();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
