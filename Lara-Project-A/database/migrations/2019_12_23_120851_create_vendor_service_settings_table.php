<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateVendorServiceSettingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('vendor_service_settings', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('vendor_id')->index();
            $table->integer('service_id')->index();
            $table->float('small');
            $table->float('medium');
            $table->float('large');
            $table->float('premium');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_service_settings');
    }
}
