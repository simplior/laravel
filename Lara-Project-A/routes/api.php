<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Customer APIs Routes
Route::group(['middleware' => 'check-application:customer'], function(){

  Route::post('/check-number', 'API\UserController@checkNumber');
  Route::post('/login', 'API\UserController@login');
  Route::post('/register', 'API\UserController@register');

  Route::group(['middleware' => 'auth:api'], function(){

      // Auth
      Route::get('logout', 'API\UserController@logout');
      Route::get('profile', 'API\UserController@profile');
      Route::post('update-profile', 'API\UserController@update');
      Route::post('verify-email', 'API\UserController@verifyEmail');

      // Brands & Models
      Route::get('brands', 'API\CarController@brands');
      Route::post('car-models', 'API\CarController@models');
      Route::apiResource('cars', 'API\CarController');

      // List
      Route::post('list/services', 'API\ListController@services');
      Route::post('list/services-cost', 'API\ListController@servicesCost');
      Route::post('list/vendors', 'API\ListController@vendors');
      Route::get('list/addresses', 'API\ListController@addresses');
      Route::post('list/location', 'API\ListController@location');
      Route::get('list/home-page', 'API\ListController@homePage');

      // User Addresses
      Route::apiResource('addresses', 'API\UserAddressController');

      // Orders
      Route::post('orders/{id}/payment-received', 'API\OrderController@paymentReceived');
      Route::post('orders/{id}/razorpay-payment-received', 'API\OrderController@razorpayPaymentReceived');
      Route::get('orders/{id}/status', 'API\OrderController@status');
      Route::get('orders/{id}/cancel', 'API\OrderController@cancel');
      Route::get('orders/{id}/photos', 'API\OrderController@photos');
      Route::apiResource('orders', 'API\OrderController');
      Route::post('orders/apply-promo-code', 'API\OrderController@checkPromoCode');
      Route::post('orders/payu-hash', 'API\OrderController@makeHash');
      Route::post('orders/pay-now', 'API\OrderController@payNow');

      // Notification
      Route::get('notifications', 'API\NotificationController@index');
      Route::get('notifications/count', 'API\NotificationController@getCount');
      Route::get('notifications/clear', 'API\NotificationController@clear');
      Route::post('notifications/read', 'API\NotificationController@markRead');
  });
});

// Vendor APIs Routes
Route::group(['prefix' => 'vendor', 'middleware' => 'check-application:vendor'], function(){

  Route::post('/check-number', 'API\UserController@checkNumber');
  Route::post('/login', 'API\UserController@loginVendor');
  Route::post('/reset-password', 'API\UserController@resetPassword');

  Route::group(['middleware' => 'auth:api'], function(){

      // Auth
      Route::get('logout', 'API\UserController@logout');
      Route::get('profile', 'API\UserController@profile');

      // Orders
      Route::post('orders', 'API\OrderController@vendorOrder');
      Route::post('orders/change-status', 'API\OrderController@changeStatus');
      Route::post('orders/upload', 'API\OrderController@uploadDocuments');
      Route::post('orders/recommendation/{id}', 'API\OrderController@recommendationStore');
      Route::get('orders/{id}', 'API\OrderController@show');

      // List
      Route::post('list/services', 'API\ListController@services');
      Route::get('list/declined-reasons', 'API\ListController@reasons');
      Route::get('list/pickup-persons', 'API\ListController@pickupPersons');
      Route::get('list/home-page', 'API\ListController@homePageVendor');

      // Service
      Route::post('services/get-cost', 'API\OrderController@getServiceCost');

      // Notification
      Route::get('notifications', 'API\NotificationController@index');
      Route::get('notifications/count', 'API\NotificationController@getCount');
      Route::get('notifications/clear', 'API\NotificationController@clear');
      Route::post('notifications/read', 'API\NotificationController@markRead');
  });
});