<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('command/{command}', function($command){
    $exitCode = Artisan::call($command);
    $output = Artisan::output();
    dd($output);
});

Route::get('artisan/{command}', 'Controller@artisan');

Route::get('/', function () {
    return Redirect::to('dashboard');
});

Route::any('/upload', function () {
    return 'image';
});

Auth::routes();
Route::get('/user/verify/{token}', 'Admin\CustomerController@verifyUser');

Route::group(['middleware' => ['web', 'auth']], function() {
    
    // Admin and Vendor Admin can access this group of routes
    Route::group(['middleware' => 'check-access:admin|vendor_admin'], function(){
        
        // Dashboard
        Route::get('dashboard', 'Admin\DashboardController@index')->name('dashboard');

        // Cars
        Route::post('cars/getModels', 'Admin\CarController@getModelsFromBrands');

        // Reviews
        Route::resource('reviews', 'Admin\ReviewController');
        Route::post('reviews/bulk-action', 'Admin\ReviewController@bulkAction');

        // Promotions
        Route::resource('promotions', 'Admin\PromotionController');
        Route::get('promotions/{id}/delete', 'Admin\PromotionController@destroy');
        Route::post('promotions/bulk-action', 'Admin\PromotionController@bulkAction');

        // Services and SubServices
        Route::post('services/getSubServices', 'Admin\ServiceController@getSubServicesFromServices');
        Route::post('getServiceCost', 'Admin\ServiceController@getServiceCost');

        // Vendors
        Route::resource('vendor', 'Admin\VendorController');
        Route::get('vendorPickupPerson/{id}', 'Admin\VendorController@pickupPersonList');
        Route::post('vendorPickupPerson', 'Admin\VendorController@storePickupPerson');
        Route::put('vendorPickupPerson/{id}', 'Admin\VendorController@updatePickupPerson');
        Route::get('vendorPickupPerson/{id}/delete', 'Admin\VendorController@destroyPickupPerson');
        Route::post('vendor/{id}/updateManufacturers', 'Admin\VendorController@updateManufacturers');
        Route::post('vendor/{id}/syncSegments', 'Admin\VendorController@syncSegments');
        Route::post('vendor/{id}/updateServices', 'Admin\VendorController@updateServices');

        // Orders
        Route::get('orders/create', 'Admin\OrderController@create');
        Route::get('orders/{id}/discard', 'Admin\OrderController@discardChanges');
        Route::get('orders/{id}/{mode?}', 'Admin\OrderController@show');
        Route::resource('orders', 'Admin\OrderController');
        Route::get('orders/{id}/delete', 'Admin\OrderController@destroy');
        Route::post('orders/bulk-action', 'Admin\OrderController@bulkAction');
        Route::post('orders/change-status', 'Admin\OrderController@changeStatus');
        Route::post('orders/apply-promo-code', 'Admin\OrderController@checkPromoCode');
        Route::post('orders/request-for-payment', 'Admin\OrderController@requestForPayment');

        // Users
        Route::resource('users', 'Admin\UserController');
        Route::get('users/{id}/delete', 'Admin\UserController@destroy');
        Route::post('users/bulk-action', 'Admin\UserController@bulkAction');
        Route::post('users/change-password', 'Admin\UserController@changePassword')->name('users.change-password');
    });

    // Only Admin can access this group of routes
    Route::group(['middleware' => 'check-access:admin'], function(){
        
        // Cars => Car = CarModel
        Route::resource('cars', 'Admin\CarController');
        Route::get('cars/{type}/{id}/delete', 'Admin\CarController@destroy');
        Route::post('cars/bulk-action', 'Admin\CarController@bulkAction');
        Route::post('cars/changeSegment', 'Admin\CarController@syncSegment');
        Route::post('carList', 'Admin\CarController@carList');

        // Services and SubServices
        Route::resource('services', 'Admin\ServiceController');
        Route::get('services/{id}/delete', 'Admin\ServiceController@destroy');
        Route::post('services/bulk-action', 'Admin\ServiceController@bulkAction');
        Route::post('serviceList', 'Admin\ServiceController@serviceList');

        // Customers
        Route::resource('customers', 'Admin\CustomerController');
        Route::get('customers/{id}/delete', 'Admin\CustomerController@destroy');
        Route::post('customers/bulk-action', 'Admin\CustomerController@bulkAction');
        Route::post('customerList', 'Admin\CustomerController@customerList');
        Route::post('customerCars', 'Admin\CustomerController@storeCar');
        Route::put('customerCars/{id}', 'Admin\CustomerController@updateCar');
        Route::get('customerCars/{id}/delete', 'Admin\CustomerController@destroyCar');
        Route::post('customerAddressList', 'Admin\CustomerController@customerAddressList');
        Route::post('customerAddresses', 'Admin\CustomerController@storeAddress');
        Route::put('customerAddresses/{id}', 'Admin\CustomerController@updateAddress');
        Route::get('customerAddresses/{id}/delete', 'Admin\CustomerController@destroyAddress');
        Route::get('customerAddresses/{id}/primary', 'Admin\CustomerController@primaryAddress');
        Route::post('location', 'Admin\CustomerController@location');

        // Vendors
        Route::get('vendor/{id}/delete', 'Admin\VendorController@destroy');
        Route::post('vendor/bulk-action', 'Admin\VendorController@bulkAction');

        // Migration
        Route::group(['prefix' => 'migration'], function(){
            Route::get('brands', 'Admin\MigrationController@brands');
        });
    });

    // Logout call
	Route::any('/logout', 'Auth\LoginController@logout')->name('logout');
});