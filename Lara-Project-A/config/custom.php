<?php
return [
    'custom' => [
        'theme' => 'light',					    // options[String]: 'light'(default), 'dark', 'semi-dark'
        'sidebarCollapsed' => false,		// options[Boolean]: true, false(default)
        'navbarColor' => '',			      // options[String]: bg-primary, bg-info, bg-warning, bg-success, bg-danger, bg-dark (default: '' for #fff)
        'menuType' => 'fixed',			  // options[String]: fixed(default) / static
        'navbarType' => 'sticky',			// options[String]: floating(default) / static / sticky / hidden
        'footerType' => 'static',				// options[String]: static(default) / sticky / hidden
        'bodyClass' => '',              // add custom class
        'pageHeader' => true,           // options[Boolean]: true(default), false (Page Header for Breadcrumbs)
        'contentLayout' => '',          // options[String]: "" (default), content-left-sidebar, content-right-sidebar, content-detached-left-sidebar, content-detached-right-sidebar
        'blankPage' => false,            // options[Boolean]: true, false(default)
        'mainLayoutType' => 'horizontal',          // Options[String]: horizontal, vertical(default)
        'direction' => env('MIX_CONTENT_DIRECTION', 'ltr') // Options[String]: ltr(default), rtl
    ],

    'carsseva' => [

        // Roles
        'roles' => array(
            array('id' => 'vendor_admin', 'name' => 'Vendor Admin'),
            //array('id' => 'manager', 'name' => 'Manager'),
            array('id' => 'pickup_person', 'name' => 'Pickup Person'),
        ),

        // All Order Status
        'all_order_status' => array(
            array('id' => 'order_received', 'name' => 'Order Received'),
            array('id' => 'order_confirmed', 'name' => 'Order Confirmed'),
            array('id' => 'order_cancelled', 'name' => 'Order Cancelled'),
            array('id' => 'team_enroute', 'name' => 'Team Enroute'),
            array('id' => 'car_picked_up', 'name' => 'Car Picked Up'),
            array('id' => 'service_started', 'name' => 'Service Started'),
            array('id' => 'service_completed', 'name' => 'Service Completed'),
            array('id' => 'delivered', 'name' => 'Delivered'),
        ),

        // Allow Status
        'allow_status' => array(
            'order_received' => array(
                array('id' => 'order_received', 'name' => 'Order Received'),
                array('id' => 'order_confirmed', 'name' => 'Order Confirmed'),
                array('id' => 'order_cancelled', 'name' => 'Order Cancelled'),
            ),
            'order_confirmed' => array(
                array('id' => 'order_received', 'name' => 'Order Received'),
                array('id' => 'order_confirmed', 'name' => 'Order Confirmed'),
                array('id' => 'team_enroute', 'name' => 'Team Enroute'),
            ),
            'order_cancelled' => array(
                array('id' => 'order_confirmed', 'name' => 'Order Confirmed'),
                array('id' => 'order_cancelled', 'name' => 'Order Cancelled')
            ),
            'team_enroute' => array(
                array('id' => 'order_confirmed', 'name' => 'Order Confirmed'),
                array('id' => 'team_enroute', 'name' => 'Team Enroute'),
                array('id' => 'car_picked_up', 'name' => 'Car Picked Up'),
            ),
            'car_picked_up' => array(
                array('id' => 'team_enroute', 'name' => 'Team Enroute'),
                array('id' => 'car_picked_up', 'name' => 'Car Picked Up'),
                array('id' => 'service_started', 'name' => 'Service Started'),
            ),
            'service_started' => array(
                array('id' => 'car_picked_up', 'name' => 'Car Picked Up'),
                array('id' => 'service_started', 'name' => 'Service Started'),
                array('id' => 'service_completed', 'name' => 'Service Completed'),
            ),
            'service_completed' => array(
                array('id' => 'service_started', 'name' => 'Service Started'),
                array('id' => 'service_completed', 'name' => 'Service Completed'),
                array('id' => 'delivered', 'name' => 'Delivered'),
            ),
            'delivered' => array(
                array('id' => 'service_completed', 'name' => 'Service Completed'),
                array('id' => 'delivered', 'name' => 'Delivered'),
            ),
        ),

        // Prefix Settings
        'prefix' => [
            'admin' => 'ADM',
            'customer' => 'CSCUS',
            'vendor' => 'CSVEN',
            'order' => 'ORD',
            'transaction' => 'TRN',
        ],
    ]
];
