<p>Hello {{ $name }},</p>
<p>Here is your verification link, You can verify your email using below link.</p>
<p><a style="text-decoration: none; font-weight: bold;" href="{!! $verification_link ?? '' !!}">Click here to verify</a></p>
<p>Thank you!<br />Team - {{ $providername ?? '' }}</p>
