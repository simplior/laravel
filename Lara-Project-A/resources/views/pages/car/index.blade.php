@extends('layouts/contentLayoutMaster')

@section('title', 'Manage Cars')

@section('vendor-style')
        {{-- vendor css files --}}
        <link rel="stylesheet" href="{{ asset(mix('vendors/css/tables/datatable/datatables.min.css')) }}">
        <link rel="stylesheet" href="{{ asset(mix('css/plugins/forms/validation/form-validation.css')) }}">
        {{-- <link rel="stylesheet" href="{{ asset(mix('vendors/css/file-uploaders/dropzone.min.css')) }}"> --}}
        <link rel="stylesheet" href="{{ asset(mix('vendors/css/file-uploaders/dropzone.css')) }}">
        <link rel="stylesheet" href="{{ asset(mix('vendors/css/forms/select/select2.min.css')) }}">
@endsection

@section('page-style')
        <!-- Page css files -->
        <link rel="stylesheet" href="{{ asset(mix('css/plugins/file-uploaders/dropzone.css')) }}">
@endsection

@section('content')
        
        <!-- Column selectors with Export Options and print table -->
        <section id="column-selectors">
            <div class="row">
                <div class="col-12 col-xl-4">
                    <div class="card">
                        <div class="card-header">
                            <h4 class="card-title" id="card-title">Add Brands/Models</h4>
                        </div>
                        <div class="card-content">
                            <div class="card-body">
                                <form class="form-horizontal" id="brandModleFrm" method="post" action="{{ route('cars.store') }}" novalidate enctype="multipart/form-data">
                                    @csrf
                                    <div class="row">
                                        <div class="col-sm-12">
                                            <div class="form-group">
                                                <label for="first-name-vertical">Select Type *</label>
                                                <ul class="list-unstyled mb-0">
                                                    <li class="d-inline-block mr-2">
                                                        <fieldset>
                                                            <div class="vs-radio-con">
                                                                <input type="radio" name="type" id="brand-radio" checked value="brand">
                                                                <span class="vs-radio">
                                                                <span class="vs-radio--border"></span>
                                                                <span class="vs-radio--circle"></span>
                                                                </span>
                                                                <span class="">Brand</span>
                                                            </div>
                                                        </fieldset>
                                                    </li>
                                                    <li class="d-inline-block mr-2">
                                                        <fieldset>
                                                            <div class="vs-radio-con">
                                                                <input type="radio" name="type" id="model-radio" value="model">
                                                                <span class="vs-radio">
                                                                <span class="vs-radio--border"></span>
                                                                <span class="vs-radio--circle"></span>
                                                                </span>
                                                                <span class="">Car Model</span>
                                                            </div>
                                                        </fieldset>
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>

                                        {{-- Brand Section --}}
                                        <div class="col-sm-12 brand-section">
                                            <div class="form-group">
                                                <label for="first-name-vertical">Name *</label>
                                                <div class="controls">
                                                    <input type="text" id="name" name="name" class="form-control @error('name') is-invalid @enderror" placeholder="Name" value="{{ old('name') }}" required data-validation-required-message="This name field is required">

                                                    @error('name')
                                                        <span class="invalid-feedback help-block" role="alert">
                                                            <strong>{{ $message }}</strong>
                                                        </span>
                                                    @enderror
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label for="first-name-vertical">Images</label>
                                                <div class="controls">
                                                    <div id="brandImage" class="dropzone dropzone-area" style="min-height: 220px;"><div class="dz-message">Drop Files Here To Upload</div></div>
                                                </div>
                                            </div>
                                        </div>

                                        {{-- Model Section --}}
                                        <div class="col-sm-12 model-section" style="display: none;">
                                            <div class="form-group">
                                                <label for="first-name-vertical">Select Brand *</label>
                                                <div class="controls">
                                                    
                                                    <select name="brand_id" id="brand_id" class="form-control select2" required data-validation-required-message="This brand field is required">
                                                        <option value='' selected="selected">Select Brand</option>
                                                        @foreach($brands as $inb => $brand)
                                                            <option value="{{ $brand['id'] }}">{{ $brand['name'] }}</option>
                                                        @endforeach
                                                    </select>

                                                    @error('name')
                                                        <span class="invalid-feedback help-block" role="alert">
                                                            <strong>{{ $message }}</strong>
                                                        </span>
                                                    @enderror
                                                </div>     
                                            </div>
                                            <div class="media-list model-list-section" data-current="0">
                                                <div class="media" style="padding: 0;" id="model0">
                                                    <div class="media-body row">
                                                        <div class="col-sm-5 col-lg-5">
                                                            <div class="form-group">
                                                                <label for="first-name-vertical">Image</label>
                                                                <div class="custom-file">
                                                                    <input type="file" class="custom-file-input" name="model_image[]" id="image0">
                                                                    <label class="custom-file-label" for="image0"></label>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-sm-5 col-lg-6">
                                                            <div class="form-group">
                                                                <label for="first-name-vertical">Name *</label>
                                                                <div class="controls">
                                                                    <input type="text" name="model_name[]" id="name0" class="form-control model-name" placeholder="Name" required>
                                                                    {{-- data-rule-checkexist="true" --}}
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-sm-2 col-lg-1">
                                                            <div class="form-group">
                                                                <label for="first-name-vertical"></label>
                                                                <div class="controls add-btn">
                                                                    <i class="feather icon-plus-circle cursor-pointer add-model" style="font-size: 20px;margin-right: 5px;"></i>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                    </div>
                                    <button type="submit" class="btn btn-primary mr-1 mb-1 waves-effect waves-light">Save</button>
                                    <button type="reset" class="btn btn-outline-warning mr-1 mb-1 waves-effect waves-light add-action">Cancel</button>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-12 col-xl-8">
                    <div class="card">
                        <div class="card-header">
                            <h4 class="card-title">Manage Brands/Models</h4>
                        </div>
                        <div class="card-content">
                            <div class="card-body card-dashboard">
                                <div class="table-responsive">
                                    <table class="table brand-table compact-table table-data-nowrap">
                                        <thead>
                                            <tr>
                                                <th rowspan="2">Sr</th>
                                                <th rowspan="2">Image</th>
                                                <th rowspan="2">Name</th>
                                                <th rowspan="2">Model</th>
                                                <th colspan="4" class="text-center">Segments</th>
                                                <th rowspan="2">Status</th>
                                                <th rowspan="2">Action</th>
                                            </tr>
                                            <tr>
                                                <th>S</th>
                                                <th>M</th>
                                                <th>L</th>
                                                <th>P</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                          @php
                                            $sr = 1;
                                          @endphp
                                          @foreach($brands as $in => $brand)
                                            <tr class="group">
                                                <td>{{ $sr }}</td>
                                                <td>
                                                  <ul class="list-unstyled users-list m-0 d-flex align-items-center">
                                                    <li data-toggle="tooltip" data-popup="tooltip-custom" data-placement="bottom" data-original-title="{{ $brand->name }}" class="avatar pull-up">
                                                      <img class="media-object rounded-circle" src="{{ $brand->image_url }}" alt="Avatar" height="30" width="30">
                                                    </li>
                                                  </ul>
                                                </td>
                                                <td>{{ $brand->name }}</td>
                                                <td>{{ $brand->brand }}</td>
                                                <td></td>
                                                <td></td>
                                                <td></td>
                                                <td></td>
                                                <td>
                                                    <div class="custom-control custom-switch custom-switch-success custom-control-inline">
                                                        <input type="checkbox" class="custom-control-input item-status-change" data-item="{{ $brand }}" data-sr="status{{ $sr }}" id="status{{ $sr }}" {{ (($brand->status == '1') ? 'checked="checked"' : '') }}>
                                                        <label class="custom-control-label" for="status{{ $sr }}"></label>
                                                    </div>
                                                </td>
                                                <td class="action-btn">
                                                  <button type="button" class="btn btn-sm btn-icon btn-icon rounded-circle btn-flat-light mr-1 mb-1 waves-effect waves-light edit-action" data-item="{{ $brand }}"><i class="feather icon-edit"></i></button>
                                                  <button type="button" class="btn btn-sm btn-icon btn-icon rounded-circle btn-flat-light mr-1 mb-1 waves-effect waves-light delete-item" data-item="{{ $brand }}"><i class="feather icon-trash-2"></i></button>
                                                </td>
                                            </tr>
                                            @php
                                                $sr += 1;
                                            @endphp
                                            @if ( isset($brand->models) && count($brand->models) > 0 )
                                                @foreach($brand->models as $inm => $model)
                                                    <tr>
                                                        <td>{{ $sr }}</td>
                                                        <td>
                                                          <ul class="list-unstyled users-list m-0 d-flex align-items-center">
                                                            <li data-toggle="tooltip" data-popup="tooltip-custom" data-placement="bottom" data-original-title="{{ $model->name }}" class="avatar pull-up">
                                                              <img class="media-object rounded-circle" src="{{ $model->image_url }}" alt="Avatar" height="30" width="30">
                                                            </li>
                                                          </ul>
                                                        </td>
                                                        <td>{{ $model->name }}</td>
                                                        <td>{{ $brand->name }}</td>
                                                        <td>
                                                            <fieldset>
                                                                <div class="vs-radio-con vs-radio-light">
                                                                    <input type="radio" class="modelRadio" data-model="{{ $model }}" name="model{{ $model->id }}" {{ ((isset($model->segment->segment) && $model->segment->segment == 'small' ) ? 'checked="checked"' : '') }} value="small">
                                                                    <span class="vs-radio vs-radio-sm">
                                                                      <span class="vs-radio--border"></span>
                                                                      <span class="vs-radio--circle"></span>
                                                                    </span>
                                                                </div>
                                                            </fieldset>
                                                        </td>
                                                        <td>
                                                            <fieldset>
                                                                <div class="vs-radio-con vs-radio-light">
                                                                    <input type="radio" class="modelRadio" data-model="{{ $model }}" name="model{{ $model->id }}" {{ ((isset($model->segment->segment) && $model->segment->segment == 'medium' ) ? 'checked="checked"' : '') }} value="medium">
                                                                    <span class="vs-radio vs-radio-sm">
                                                                      <span class="vs-radio--border"></span>
                                                                      <span class="vs-radio--circle"></span>
                                                                    </span>
                                                                </div>
                                                            </fieldset>
                                                        </td>
                                                        <td>
                                                            <fieldset>
                                                                <div class="vs-radio-con vs-radio-light">
                                                                    <input type="radio" class="modelRadio" data-model="{{ $model }}" name="model{{ $model->id }}" {{ ((isset($model->segment->segment) && $model->segment->segment == 'large' ) ? 'checked="checked"' : '') }} value="large">
                                                                    <span class="vs-radio vs-radio-sm">
                                                                      <span class="vs-radio--border"></span>
                                                                      <span class="vs-radio--circle"></span>
                                                                    </span>
                                                                </div>
                                                            </fieldset>
                                                        </td>
                                                        <td>
                                                            <fieldset>
                                                                <div class="vs-radio-con vs-radio-light">
                                                                    <input type="radio" class="modelRadio" data-model="{{ $model }}" name="model{{ $model->id }}" {{ ((isset($model->segment->segment) && $model->segment->segment == 'premium' ) ? 'checked="checked"' : '') }} value="premium">
                                                                    <span class="vs-radio vs-radio-sm">
                                                                      <span class="vs-radio--border"></span>
                                                                      <span class="vs-radio--circle"></span>
                                                                    </span>
                                                                </div>
                                                            </fieldset>
                                                        </td>
                                                        <td>
                                                            <div class="custom-control custom-switch custom-switch-success custom-control-inline">
                                                                <input type="checkbox" class="custom-control-input item-status-change" data-item="{{ $model }}" data-sr="status{{ $sr }}" id="status{{ $sr }}" {{ (($model->status == '1') ? 'checked="checked"' : '') }}>
                                                                <label class="custom-control-label" for="status{{ $sr }}"></label>
                                                            </div>
                                                        </td>
                                                        <td class="action-btn">
                                                          <button type="button" class="btn btn-sm btn-icon btn-icon rounded-circle btn-flat-light mr-1 mb-1 waves-effect waves-light edit-action" data-item="{{ $model }}"><i class="feather icon-edit"></i></button>
                                                          <button type="button" class="btn btn-icon btn-icon rounded-circle btn-flat-light mr-1 mb-1 waves-effect waves-light delete-item" data-item="{{ $model }}"><i class="feather icon-trash-2"></i></button>
                                                        </td>
                                                    </tr>
                                                    @php
                                                        $sr += 1;
                                                    @endphp
                                                @endforeach
                                            @endif
                                          @endforeach
                                        </tbody>
                                        <tfoot>
                                            <tr>
                                                <th>Sr</th>
                                                <th>Image</th>
                                                <th>Name</th>
                                                <th>Model</th>
                                                <th colspan="4" class="text-center">Segments</th>
                                                <th>Status</th>
                                                <th>Action</th>
                                            </tr>
                                        </tfoot>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!-- Column selectors with Export Options and print table -->

        @section('carsseva-alert')

            <!-- Alerts -->
            @foreach (['danger', 'warning', 'success', 'info'] as $key)
                @if(Session::has($key))
                    <div class="cws-alert">
                        <div class="alert alert-{{ $key }} alert-dismissible alert-auto-hide fade show" role="alert">
                            <div class="alert-text">{{ Session::get($key) }}</div>
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true"><i class="feather icon-x-circle"></i></span>
                            </button>
                        </div>
                    </div>
                @endif
            @endforeach

            <!-- Confirmation alert msg -->
            <div class="cws-alert car-alert" style="display: none;">
                <div class="background-overlay car-reject"></div>
                <div class="alert alert-dark-danger alert-dismissible fade show" role="alert">
                    <div class="alert-text">Are you sure you want to delete <strong class="car-name"></strong>? 
                        <div class="alert-btn">
                            <a href="#" class="btn btn-primary car-confirm">Yes</a>
                            <button class="btn btn-white car-reject">Cancel</button>
                        </div>
                    </div>
                    <button type="button" class="close car-reject">
                        <span aria-hidden="true"><i class="feather icon-x-circle"></i></span>
                    </button>
                </div>
            </div>

            <!-- Confirmation alert msg for status change -->
            <div class="cws-alert status-change-alert" style="display: none;">
                <div class="background-overlay status-change-reject"></div>
                <div class="alert alert-dark-danger alert-dismissible fade show" role="alert">
                    <div class="alert-text">Are you sure you want to <strong class="status-change-name"></strong>? 
                        <div class="alert-btn">
                            <button class="btn btn-primary status-change-confirm">Yes</button>
                            <button class="btn btn-white status-change-reject">Cancel</button>
                        </div>
                    </div>
                    <button type="button" class="close status-change-reject">
                        <span aria-hidden="true"><i class="feather icon-x-circle"></i></span>
                    </button>
                </div>
            </div>

        @endsection

@endsection
@section('vendor-script')
        {{-- vendor files --}}
        <script src="{{ asset(mix('vendors/js/tables/datatable/datatables.min.js')) }}"></script>
        <script src="{{ asset(mix('vendors/js/tables/datatable/datatables.bootstrap4.min.js')) }}"></script>
        <script src="{{ asset(mix('vendors/js/forms/validation/jquery.validate.min.js')) }}"></script>
        {{-- <script src="{{ asset(mix('vendors/js/extensions/dropzone.min.js')) }}"></script> --}}
        <script src="{{ asset(mix('vendors/js/extensions/dropzone.js')) }}"></script>
        <script src="{{ asset(mix('vendors/js/forms/select/select2.full.min.js')) }}"></script>
@endsection
@section('page-script')
    
        {{-- Page js files --}}
        <script type="text/javascript">
            
            
            Dropzone.autoDiscover = false;
            // File Upload
            var myDropzone = new Dropzone('#brandImage', {
                url: "{{ URL::to('upload') }}",
                paramName: "image",
                addRemoveLinks: true,
                acceptedFiles: 'image/*',
                maxFiles:1,
                init: function () {
                    this.on("maxfilesexceeded", function(file) {
                        this.removeAllFiles();
                        this.addFile(file);
                    });
                },
                success: function (file, response) {
                    file.previewElement.classList.add("dz-success");
                    $('#brandModleFrm').append('<input type="hidden" name="image_file" id="image_file" value="' + file.dataURL + '">');
                },
                reset: function () {
                    $('#image').val('');
                    $('#image_file').val('');
                },
                error: function (file, response) {
                    file.previewElement.classList.add("dz-error");
                    $('#image').val('');
                    $('#image_file').val('');
                }
            });

            $(document).ready(function() {

                var validator = $( "#brandModleFrm" ).validate();

                $.validator.addMethod("checkexist", function(value, element) {
                    var models = '{!! $models_name !!}';
                    if ( $.inArray(value.toLowerCase(), jQuery.parseJSON(models)) > -1 ) {
                        return false;
                    } else {
                        return true;
                    }
                }, "Model name already exist");

                $('.select2').select2({
                    dropdownAutoWidth: true,
                    width: '100%',
                    placeholder: "Select Brand"
                });

                $('input[name=type]').change(function() {
                    if (this.value == 'brand') {
                        $('.brand-section').show();
                        $('.model-section').hide();
                    } else {
                        $('.brand-section').hide();
                        $('.model-section').show();
                    }
                });
                
                // Datatable
                $('.brand-table').DataTable({
                    "processing": true,
                    "ordering": false,
                    "columnDefs": [
                        { "visible": false, "targets": 0},
                        { "width": '8%', "targets": 1},
                        { "width": '170px', "targets": 2},
                        { "visible": false, "targets": 3},
                        { "width": '8%', "targets": 4},
                        { "width": '8%', "targets": 5},
                        { "width": '8%', "targets": 6},
                        { "width": '8%', "targets": 7},
                        { "width": '8%', "targets": 8},
                        { "width": '15%', "targets": 9},
                    ],
                    "order": [
                        [0, 'asc']
                    ]
                });

                // Add Form
                $('.add-action').click(function(){
                    
                    $('#type').remove();
                    $('#_method').remove();
                    $("#card-title").html('Add Brands/Models');
                    $('#brandModleFrm').attr('action', 'cars');
                    $('input[name=type]').attr("disabled", false);
                    $("#brand-radio").prop("checked", true);
                    $('#brandModleFrm')[0].reset();

                    $('.dz-remove')[0].click();
                });

                $('.car-reject').click( function() {
                    $('.car-alert').hide();
                });

                // Status change
                $('.status-change-confirm').click( function() {
                    
                    // trigger status-change event
                    var ids = $(this).attr('data-ids');
                    var action = $(this).attr('data-action');
                    var type = $(this).attr('data-type');

                    $.post('cars/bulk-action', {'action': action, 'ids': ids, 'type': type}, function(response){
                        if ( response.status == 200 ) {
                            window.location = "{{ url('/cars') }}"
                        }
                    });
                });

                $('.status-change-reject').click( function() {

                    var action = $(this).attr('data-action');
                    var sr = $(this).attr('data-sr');
                    
                    if ( action == 'active' ) {
                        $("#"+sr).prop("checked", false);
                    } else {
                        $("#"+sr).prop("checked", true);
                    }

                    $('.status-change-alert').hide();
                });
            });

            // Segment Change
            $(document).on('change', 'input[type=radio][class=modelRadio]', function() {
                var model = $(this).data('model');

                $.post('cars/changeSegment', {'car_model_id': model.id, 'segment': this.value}, function(response){
                    if ( response.status == 200 ) {
                        //window.location = "{{ url('/cars') }}"
                    }
                });
            });

            // Edit Form
            $(document).on('click', '.edit-action', function() {

                var item = $(this).data('item');

                $('#brandModleFrm').attr('action', 'cars/'+item.id);
                $('input[name=type]').attr("disabled", true);
                $('#brandModleFrm').append('<input name="_method" id="_method" type="hidden" value="PUT">');

                if ( item.brand_id != undefined ) {
                    $('#brandModleFrm').append('<input type="hidden" name="type" id="type" value="model">');
                    $('#brandModleFrm').append('<input type="hidden" name="brand_id" value="'+item.brand_id+'">');

                    $("#card-title").html('Edit '+item.name+' Model');
                    $("#model-radio").prop("checked", true);
                    $(".brand-section").hide();
                    $(".model-section").show();

                    $("#brand_id").val(item.brand_id).trigger('change');
                    $("#brand_id").select2({disabled: 'readonly'});
                    $("#name0").val(item.name);
                    $('#brandModleFrm').append('<input type="hidden" name="image_file" id="image_file" value="'+item.image+'">');

                } else {
                    $('#brandModleFrm').append('<input type="hidden" name="type" id="type" value="brand">');
                    $("#card-title").html('Edit '+item.name+' Brand');
                    $("#brand-radio").prop("checked", true);
                    $("#name").val(item.name);
                    $(".model-section").hide();
                    $(".brand-section").show();

                    // Edit Image
                    if ( item.image != '' ) {
                        
                        if ( $('.dz-remove')[0] != undefined ) {
                            $('.dz-remove')[0].click();
                        }
                        var mockFile = { name: item.image };
                        myDropzone.options.addedfile.call(myDropzone, mockFile);
                        myDropzone.options.thumbnail.call(myDropzone, mockFile, item.image_url);
                        mockFile.previewElement.classList.add('dz-success');
                        mockFile.previewElement.classList.add('dz-complete');
                        $('#brandModleFrm').append('<input type="hidden" name="image_file" id="image_file" value="'+item.image+'">');
                    }
                }

            });

            // Delete Form
            $(document).on('click', '.delete-item', function() {
                var item = $(this).data('item');

                if ( item.brand_id != undefined ) {
                    item.type = 'model';
                    $('.car-confirm').attr('href', "{{ URL::to('cars') }}/model/"+item.id+"/delete");
                    $('.car-name').html(item.name + ' model');
                } else {
                    item.type = 'brand';
                    $('.car-confirm').attr('href', "{{ URL::to('cars') }}/brand/"+item.id+"/delete");
                    $('.car-name').html(item.name + ' brand');
                }
                $('.car-alert').show();
            });

            $(document).on('change', '.item-status-change', function() {
                var item = $(this).data('item');
                var status = ((this.checked) ? 'active' : 'inactive');
                
                if ( item.brand_id != undefined ) {
                    $('.status-change-confirm').attr('data-ids', item.id);
                    $('.status-change-confirm').attr('data-type', 'model');
                } else {
                    $('.status-change-confirm').attr('data-ids', item.id);
                    $('.status-change-confirm').attr('data-type', 'brand');
                }

                $('.status-change-name').html(status + ' ' + item.name);
                $('.status-change-alert').show();
                $('.status-change-confirm').attr('data-action', status);
                $('.status-change-reject').attr('data-action', status);
                $('.status-change-reject').attr('data-sr', $(this).data('sr'));
            });
            
            $(document).on('click', '.add-model', function() {
                var current = $('.model-list-section').attr('data-current');
                var next = parseFloat(current) + 1;

                var frmHtml = '';
                frmHtml += '<div class="media" style="padding: 0;" id="model'+next+'">';
                    frmHtml += '<div class="media-body row">';
                        frmHtml += '<div class="col-sm-5 col-lg-5">';
                            frmHtml += '<div class="form-group">';
                                frmHtml += '<label>Image</label>';
                                frmHtml += '<div class="custom-file">';
                                    frmHtml += '<input type="file" class="custom-file-input" name="model_image['+next+']" id="image'+next+'">';
                                    frmHtml += '<label class="custom-file-label" for="image'+next+'"></label>';
                                frmHtml += '</div>';
                            frmHtml += '</div>';
                        frmHtml += '</div>';
                        frmHtml += '<div class="col-sm-5 col-lg-6">';
                            frmHtml += '<div class="form-group">';
                                frmHtml += '<label for="first-name-vertical">Name *</label>';
                                frmHtml += '<div class="controls">';
                                    frmHtml += '<input type="text" name="model_name['+next+']" class="form-control" placeholder="Name" required data-validation-required-message="This name field is required">';
                                frmHtml += '</div>';
                            frmHtml += '</div>';
                        frmHtml += '</div>';
                        frmHtml += '<div class="col-sm-2 col-lg-1">';
                            frmHtml += '<div class="form-group">';
                                frmHtml += '<label for="first-name-vertical"></label>';
                                frmHtml += '<div class="controls add-btn">';
                                    frmHtml += '<i class="feather icon-plus-circle cursor-pointer add-model" style="font-size: 20px;margin-right: 5px;"></i>';
                                    frmHtml += '<i class="feather icon-x-circle cursor-pointer remove-model" data-current="'+next+'" style="font-size: 20px;"></i>';
                                frmHtml += '</div>';
                            frmHtml += '</div>';
                        frmHtml += '</div>';
                    frmHtml += '</div>';
                frmHtml += '</div>';

                $('.model-list-section').append(frmHtml);
                $('.model-list-section').attr('data-current', next);
            });

            $(document).on('click', '.remove-model', function() {
                var current = $(this).data('current');
                $('#model'+current).remove();
            });

        </script>

@endsection
