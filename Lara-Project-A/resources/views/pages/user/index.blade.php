@extends('layouts/contentLayoutMaster')

@section('title', 'Users')

@section('vendor-style')
        {{-- vendor css files --}}
        <link rel="stylesheet" href="{{ asset(mix('vendors/css/tables/datatable/datatables.min.css')) }}">
        <link rel="stylesheet" href="{{ asset(mix('css/plugins/forms/validation/form-validation.css')) }}">
        <link rel="stylesheet" href="{{ asset(mix('vendors/css/forms/select/select2.min.css')) }}">
@endsection

@section('page-style')
        <!-- Page css files -->
@endsection

@section('content')
        
        <!-- Column selectors with Export Options and print table -->
        <section id="column-selectors">
            <div class="row">
                <div class="col-12 col-lg-12 col-xl-5">
                    <div class="card">
                        <div class="card-header">
                            <h4 class="card-title" id="card-title">Add User</h4>
                        </div>
                        <div class="card-content">
                            <div class="card-body">
                                <form class="form-horizontal" id="userModleFrm" method="post" action="{{ route('users.store') }}" novalidate enctype="multipart/form-data">
                                    @csrf
                                    <div class="row">
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <label for="first-name-vertical">Select Role *</label>
                                                <div class="controls">
                                                    
                                                    <select name="role" id="role" class="form-control select2" required>
                                                        <option value='' selected="selected">Select Role</option>
                                                        @foreach($roles as $inb => $role)
                                                            <option value="{{ $role['id'] }}">{{ $role['name'] }}</option>
                                                        @endforeach
                                                    </select>

                                                    @error('role')
                                                        <span class="invalid-feedback help-block" role="alert">
                                                            <strong>{{ $message }}</strong>
                                                        </span>
                                                    @enderror
                                                </div>     
                                            </div>
                                        </div>
                                        @if ( isset($currentUser['role']) && $currentUser['role'] == 'admin' )
                                            <div class="col-sm-6">
                                                <div class="form-group">
                                                    <label for="first-name-vertical">Select Vendor *</label>
                                                    <div class="controls">
                                                        
                                                        <select name="vendor_id" id="vendor_id" class="form-control select2" required>
                                                            <option value='' selected="selected">Select Vendor</option>
                                                            @foreach($vendors as $inb => $vendor)
                                                                <option value="{{ $vendor['id'] }}">{{ $vendor['name'] }}</option>
                                                            @endforeach
                                                        </select>

                                                        @error('vendor_id')
                                                            <span class="invalid-feedback help-block" role="alert">
                                                                <strong>{{ $message }}</strong>
                                                            </span>
                                                        @enderror
                                                    </div>     
                                                </div>
                                            </div>
                                        @endif
                                    </div>
                                    <div class="row">
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <label for="first-name-vertical">Name *</label>
                                                <div class="controls">
                                                    <input type="text" id="name" name="name" class="form-control @error('name') is-invalid @enderror" placeholder="Name" value="{{ old('name') }}" required>

                                                    @error('name')
                                                        <span class="invalid-feedback help-block" role="alert">
                                                            <strong>{{ $message }}</strong>
                                                        </span>
                                                    @enderror
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <label for="first-name-vertical">Contact No *</label>
                                                <div class="controls">
                                                    <input type="text" id="contact_no" name="contact_no" class="form-control @error('contact_no') is-invalid @enderror" placeholder="Contact No" value="{{ old('contact_no') }}" required>

                                                    @error('contact_no')
                                                        <span class="invalid-feedback help-block" role="alert">
                                                            <strong>{{ $message }}</strong>
                                                        </span>
                                                    @enderror
                                                </div>
                                            </div>
                                        </div>
                                        <div class="clearfix"></div>
                                        <div class="col-sm-6 password-fields">
                                            <div class="form-group">
                                                <label for="first-name-vertical">Password *</label>
                                                <div class="controls">
                                                    <input type="password" id="password" name="password" class="form-control @error('password') is-invalid @enderror" placeholder="Password" required>

                                                    @error('password')
                                                        <span class="invalid-feedback help-block" role="alert">
                                                            <strong>{{ $message }}</strong>
                                                        </span>
                                                    @enderror
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-sm-6 password-fields">
                                            <div class="form-group">
                                                <label for="first-name-vertical">Retype Password *</label>
                                                <div class="controls">
                                                    <input type="password" id="password_confirmation" name="password_confirmation" class="form-control @error('password_confirmation') is-invalid @enderror" placeholder="Retype Password" required>

                                                    @error('password_confirmation')
                                                        <span class="invalid-feedback help-block" role="alert">
                                                            <strong>{{ $message }}</strong>
                                                        </span>
                                                    @enderror
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <button type="submit" class="btn btn-primary mr-1 mb-1 waves-effect waves-light">Save</button>
                                    <button type="reset" class="btn btn-outline-warning mr-1 mb-1 waves-effect waves-light add-action">Cancel</button>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-12 col-lg-12 col-xl-7">
                    <div class="card">
                        <div class="card-header">
                            <h4 class="card-title">Users</h4>
                        </div>
                        <div class="card-content">
                            <div class="card-body card-dashboard">
                                <div class="table-responsive">
                                    <table class="table user-table compact-table table-data-nowrap">
                                        <thead>
                                            <tr>
                                                <th>Sr</th>
                                                <th>Name</th>
                                                <th>Role</th>
                                                <th>Contact No</th>
                                                <th>Email</th>
                                                <th>Vendor</th>
                                                <th>Status</th>
                                                <th>Action</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                          @php
                                            $sr = 1;
                                          @endphp
                                          @foreach($users as $in => $user)
                                            <tr>
                                                <td>{{ $sr }}</td>
                                                <td>{{ $user->name }}</td>
                                                <td>{{ ucwords(str_replace('_', ' ', $user->role)) }}</td>
                                                <td>{{ $user->contact_no }}</td>
                                                <td>{{ $user->email }}</td>
                                                <td>
                                                    @if ( $user->role != 'admin' )
                                                        {{ $user->vendor->code }}
                                                    @else 
                                                        -
                                                    @endif
                                                </td>
                                                <td>
                                                    <div class="custom-control custom-switch custom-switch-success custom-control-inline">
                                                        <input type="checkbox" class="custom-control-input item-status-change" data-item="{{ $user }}" data-sr="status{{ $sr }}" id="status{{ $in }}" {{ (($user->status == '1') ? 'checked="checked"' : '') }}>
                                                        <label class="custom-control-label" for="status{{ $in }}"></label>
                                                    </div>
                                                </td>
                                                <td class="action-btn">
                                                    <button type="button" class="btn btn-sm btn-icon btn-icon rounded-circle btn-flat-light mr-1 mb-1 waves-effect waves-light" data-toggle="modal" data-target="#default" data-id="{{ $user }}"><i class="feather icon-lock"></i></button>
                                                    <button type="button" class="btn btn-sm btn-icon btn-icon rounded-circle btn-flat-light mr-1 mb-1 waves-effect waves-light edit-action" data-item="{{ $user }}"><i class="feather icon-edit"></i></button>
                                                    <button type="button" class="btn btn-sm btn-icon btn-icon rounded-circle btn-flat-light mr-1 mb-1 waves-effect waves-light delete-item" data-item="{{ $user }}"><i class="feather icon-trash-2"></i></button>
                                                </td>
                                            </tr>
                                          @endforeach
                                        </tbody>
                                        <tfoot>
                                            <tr>
                                                <th>Sr</th>
                                                <th>Name</th>
                                                <th>Role</th>
                                                <th>Contact No</th>
                                                <th>Email</th>
                                                <th>Vendor</th>
                                                <th>Status</th>
                                                <th>Action</th>
                                            </tr>
                                        </tfoot>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!-- Column selectors with Export Options and print table -->

        @section('carsseva-alert')

            <!-- Alerts -->
            @foreach (['danger', 'warning', 'success', 'info'] as $key)
                @if(Session::has($key))
                    <div class="cws-alert">
                        <div class="alert alert-{{ $key }} alert-dismissible alert-auto-hide fade show" role="alert">
                            <div class="alert-text">{{ Session::get($key) }}</div>
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true"><i class="feather icon-x-circle"></i></span>
                            </button>
                        </div>
                    </div>
                @endif
            @endforeach

            <!-- Confirmation alert msg -->
            <div class="cws-alert user-alert" style="display: none;">
                <div class="background-overlay user-reject"></div>
                <div class="alert alert-dark-danger alert-dismissible fade show" role="alert">
                    <div class="alert-text">Are you sure you want to delete <strong class="user-name"></strong>? 
                        <div class="alert-btn">
                            <a href="#" class="btn btn-primary user-confirm">Yes</a>
                            <button class="btn btn-white user-reject">Cancel</button>
                        </div>
                    </div>
                    <button type="button" class="close user-reject">
                        <span aria-hidden="true"><i class="feather icon-x-circle"></i></span>
                    </button>
                </div>
            </div>

            <!-- Confirmation alert msg for status change -->
            <div class="cws-alert status-change-alert" style="display: none;">
                <div class="background-overlay status-change-reject"></div>
                <div class="alert alert-dark-danger alert-dismissible fade show" role="alert">
                    <div class="alert-text">Are you sure you want to <strong class="status-change-name"></strong>? 
                        <div class="alert-btn">
                            <button class="btn btn-primary status-change-confirm">Yes</button>
                            <button class="btn btn-white status-change-reject">Cancel</button>
                        </div>
                    </div>
                    <button type="button" class="close status-change-reject">
                        <span aria-hidden="true"><i class="feather icon-x-circle"></i></span>
                    </button>
                </div>
            </div>

        @endsection

        {{-- Modal --}}
        <div class="modal fade text-left" id="default" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1" aria-hidden="true">
            <div class="modal-dialog modal-dialog-scrollable" role="document">
            <div class="modal-content">
                <div class="modal-header">
                <h4 class="modal-title">Change Password</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                </div>
                <form class="form-horizontal" id="changePassword" method="post" action="{{ route('users.change-password') }}" novalidate enctype="multipart/form-data">
                    @csrf
                    <input type="hidden" id="user_id" name="user_id" value="">
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label for="first-name-vertical">Password *</label>
                                    <div class="controls">
                                        <input type="password" id="password1" name="password" class="form-control @error('password') is-invalid @enderror" placeholder="Password" required>

                                        @error('password')
                                            <span class="invalid-feedback help-block" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label for="first-name-vertical">Retype Password *</label>
                                    <div class="controls">
                                        <input type="password" id="password_confirmation1" name="password_confirmation" class="form-control @error('password_confirmation') is-invalid @enderror" placeholder="Retype Password" required>

                                        @error('password_confirmation')
                                            <span class="invalid-feedback help-block" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button class="btn btn-primary">Change</button>
                    </div>
                </form>
            </div>
            </div>
        </div>

@endsection

@section('vendor-script')
        {{-- vendor files --}}
        <script src="{{ asset(mix('vendors/js/tables/datatable/datatables.min.js')) }}"></script>
        <script src="{{ asset(mix('vendors/js/tables/datatable/datatables.bootstrap4.min.js')) }}"></script>
        <script src="{{ asset(mix('vendors/js/forms/validation/jquery.validate.min.js')) }}"></script>
        <script src="{{ asset(mix('vendors/js/forms/select/select2.full.min.js')) }}"></script>

        <script type="text/javascript" src="https://cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
        <script type="text/javascript" src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>
@endsection

@section('page-script')
        {{-- Page js files --}}
        <script type="text/javascript">
            $(document).ready(function() {

                // Form
                var validator = $( "#userModleFrm" ).validate({
                    //ignore: ""
                });

                // Form
                var validator = $( "#changePassword" ).validate({
                    ignore: "",
                    rules: {
                        password: "required",
                        password_confirmation: {
                            equalTo: "#password1"
                        }
                    }
                });

                $('#range').daterangepicker({
                    timePicker: false,
                    startDate: moment().format('DD/MM/YYYY'),
                    endDate: moment().format('DD/MM/YYYY'),
                    locale: {
                      format: 'DD/MM/YYYY'
                    }
                });

                $('.select2').select2({
                    dropdownAutoWidth: true,
                    width: '100%'
                });

                // Datatable
                $('.user-table').DataTable({
                    "ordering": false,
                    "columnDefs": [
                        { "visible": false, "targets": 0},
                        { "width": '18%', "targets": 1},
                        { "width": '15%', "targets": 2},
                        { "width": '12%', "targets": 3},
                        { "width": '18%', "targets": 4},
                        { "width": '10%', "targets": 5},
                    ],
                    "order": [
                        [0, 'asc']
                    ]
                });

                // Add Form
                $('.add-action').click(function(){
                    
                    $('#_method').remove();
                    $("#card-title").html('Add User');
                    $('#userModleFrm').attr('action', 'users');
                    
                    $("#role").val([0]).trigger('change');
                    $("#vendor_id").val([0]).trigger('change');

                    $('#userModleFrm')[0].reset();
                    $(".password-fields").show();
                });

                $('.user-reject').click( function() {
                    $('.user-alert').hide();
                });

                $('.status-change-confirm').click( function() {
                    
                    // trigger status-change event
                    var ids = $(this).attr('data-ids');
                    var action = $(this).attr('data-action');

                    $.post('users/bulk-action', {'action': action, 'ids': ids}, function(response){
                        if ( response.status == 200 ) {
                            window.location = "{{ url('/users') }}"
                        }
                    });
                });

                $('.status-change-reject').click( function() {

                    var action = $(this).attr('data-action');
                    var sr = $(this).attr('data-sr');
                    
                    if ( action == 'active' ) {
                        $("#"+sr).prop("checked", false);
                    } else {
                        $("#"+sr).prop("checked", true);
                    }

                    $('.status-change-alert').hide();
                });

                $('#vendors').on('select2:select', function (e) {
                    var select = this;
                    var selections = $(select).select2('data');

                    var Values = new Array();
                    if ( e.params.data.id == 'all' ) {
                        Values.push('all');
                    } else {
                        for (var i = 0; i < selections.length; i++) {
                            if ('all' != selections[i].id) {
                                Values.push(selections[i].id);
                            }
                        }
                    }

                    $(select).val(Values).trigger('change');
                });
            });

            $('#default').on('show.bs.modal', function(e) {
                var user = $(e.relatedTarget).data('id');
                console.log(user);
                $(e.currentTarget).find('input[name="user_id"]').val(user.id);
            });

            $(document).on('click', '.edit-action', function() {
                var item = $(this).data('item');

                $('#userModleFrm').attr('action', 'users/'+item.id);
                $('#userModleFrm').append('<input name="_method" id="_method" type="hidden" value="PUT">');
                
                $("#card-title").html('Edit User');

                $("#name").val(item.name);
                $("#contact_no").val(item.contact_no);
                $("#role").val(item.role).trigger('change');
                $("#vendor_id").val(item.vendor_id).trigger('change');   

                $(".password-fields").hide();
            });

            // Status change
            $(document).on('change', '.item-status-change', function() {
                var item = $(this).data('item');
                var status = ((this.checked) ? 'active' : 'inactive');
                
                $('.status-change-confirm').attr('data-ids', item.id);

                $('.status-change-name').html(status + ' ' + item.name);
                $('.status-change-alert').show();
                $('.status-change-confirm').attr('data-action', status);
                $('.status-change-reject').attr('data-action', status);
                $('.status-change-reject').attr('data-sr', $(this).data('sr'));
            });

            // Delete Services/SubServices
            $(document).on('click', '.delete-item', function() {
                var item = $(this).data('item');
                
                $('.user-name').html(item.name);
                $('.user-confirm').attr('href', "{{ URL::to('users') }}/"+item.id+"/delete");
                $('.user-alert').show();
            });

        </script>
@endsection
