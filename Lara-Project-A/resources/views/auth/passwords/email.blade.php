@extends('layouts/fullLayoutMaster')

@section('title', 'Forgot Password')

@section('page-style')
        {{-- Page Css files --}}
        <link rel="stylesheet" href="{{ asset(mix('css/pages/authentication.css')) }}">
@endsection
@section('content')
<section class="row flexbox-container">
  <div class="col-xl-7 col-md-9 col-10 d-flex justify-content-center px-0">
      <div class="card bg-authentication rounded-0 mb-0">
          <div class="row m-0">
              <div class="col-lg-6 d-lg-block d-none text-center align-self-center">
                  <img src="{{ asset('images/pages/forgot-password.png') }}" alt="branding logo">
              </div>
              <div class="col-lg-6 col-12 p-0">
                  <div class="card rounded-0 mb-0 px-2 py-1">
                      <div class="card-header pb-1">
                          <div class="card-title">
                              <h4 class="mb-0">Recover your password</h4>
                          </div>
                      </div>
                      <p class="px-2 mb-0">Please enter your contact no and we'll send you instructions on how to reset your password.</p>
                      <div class="card-content">
                          <div class="card-body">
                            
                              <form method="POST" action="{{ route('password.email') }}">
                                @csrf
                                  <div class="form-label-group">
                                      <!-- <input type="email" id="inputEmail" class="form-control" placeholder="Email"> -->
                                      <input id="contact_no" type="number" class="form-control @error('contact_no') is-invalid @enderror" name="contact_no" value="{{ old('contact_no') }}" placeholder="Contact No" required autocomplete="contact_no" autofocus>

                                      <label for="contact_no">Contact No</label>

                                      @error('contact_no')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                      @enderror
                                  </div>

                                  <div class="float-md-left d-block mb-1">
                                    <a href="{{ URL::to('login') }}" class="btn btn-outline-primary btn-block px-75">Back to Login</a>
                                  </div>
                                  <div class="float-md-right d-block mb-1">
                                    <button type="submit" class="btn btn-primary btn-block px-75">Recover Password</button>
                                  </div>
                              </form>

                            </div>
                          </div>
                      </div>
                  </div>
              </div>
          </div>
      </div>
  </div>
</section>
@endsection

@section('carsseva-alert')

  {{-- @if (session('status'))
      <div class="cws-alert">
          <div class="alert alert-success alert-dismissible fade show" role="alert">
              <p class="mb-0">{{ session('status') }}</p>
              <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                  <span aria-hidden="true"><i class="feather icon-x-circle"></i></span>
              </button>
          </div>
      </div>
  @endif --}}

    @foreach (['danger', 'warning', 'success', 'info'] as $key)
        @if(Session::has($key))
            <div class="cws-alert">
                <div class="alert alert-{{ $key }} alert-dismissible fade show" role="alert">
                    <p class="mb-0">{{ Session::get($key) }}</p>
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true"><i class="feather icon-x-circle"></i></span>
                    </button>
                </div>
            </div>
        @endif
    @endforeach

@endsection
