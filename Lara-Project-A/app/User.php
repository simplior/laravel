<?php

namespace App;

use Laravel\Passport\HasApiTokens;
use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Storage;

class User extends Authenticatable
{
    use HasApiTokens, Notifiable;
    
    protected $appends = ['profile_pic_url'];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'vendor_id', 'sequence_id', 'code', 'role', 'name', 'email', 'contact_no', 'password', 'profile_pic', 'aadhar_card_number', 'PAN_number', 'aadhar_card', 'PAN_card', 'registered_via', 'is_name_verified', 'is_contact_no_verified', 'is_email_verified', 'social_data', 'status', 'is_deleted'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    /**
     * Route notifications for the mail channel.
     *
     * @return string
     */
    public function routeNotificationForMail()
    {
        return $this->email_id;
    }

    public function setEmail($email) {
        $this->email_id = $email;
    }

    public function getProfilePicUrlAttribute()
    {
        $value = $this->profile_pic;
        if ( $value != '' && $value != null ) {
            if ( !in_array($this->registered_via, array('facebook', 'google')) ) {
                return Storage::disk('public')->url('uploads/users/' . $value);
            } else {
                if ( strpos($value, 'https://') !== false ) {
                    return $value;
                } else {
                    return Storage::disk('public')->url('uploads/users/' . $value);
                }
            }
        } else {
            return asset('images/portrait/default.jpg');
        }
    }

    public function getPANCardUrlAttribute()
    {
        $value = $this->PAN_card;
        if ( $value != '' && $value != null ) {
            return Storage::disk('public')->url('uploads/users/'. $this->id . '/' . $value);
        } else {
            return asset('images/portrait/default.jpg');
        }
    }

    public function getAadharCardUrlAttribute()
    {
        $value = $this->aadhar_card;
        if ( $value != '' && $value != null ) {
            return Storage::disk('public')->url('uploads/users/'. $this->id . '/' . $value);
        } else {
            return asset('images/portrait/default.jpg');
        }
    }

    /**
     * Get the post that owns the comment.
     */
    public function vendor()
    {
        return $this->belongsTo('App\Models\Vendor');
    }

    /**
     * Get the comments for the blog post.
     */
    public function addresses()
    {
        return $this->hasMany('App\Models\UserAddress')
            ->where('is_deleted', '0')
            ->orderBy('id', 'desc');
    }

    /**
     * Get the comments for the blog post.
     */
    public function primary_address()
    {
        return $this->hasOne('App\Models\UserAddress')
            ->where('is_primary', '1')
            ->where('is_deleted', '0');
    }

    /**
     * Get the comments for the blog post.
     */
    public function cars()
    {
        return $this->hasMany('App\Models\UserCar')
            ->where('is_deleted', '0')
            ->orderBy('id', 'desc');
    }

    /**
     * Get the comments for the blog post.
     */
    public function orders()
    {
        return $this->hasMany('App\Models\Order', 'customer_id')
            ->orderBy('id', 'desc');
    }

    /**
     * Get the comments for the blog post.
     */
    public function active_orders()
    {
        return $this->hasMany('App\Models\Order', 'customer_id')
            ->where('status', '!=', 'delivered')
            ->orderBy('id', 'desc');
    }

    /**
     * Get the comments for the blog post.
     */
    public function completed_orders()
    {
        return $this->hasMany('App\Models\Order', 'customer_id')
            ->whereIn('status', array('service_completed', 'delivered'))
            ->orderBy('id', 'desc');
    }
}
