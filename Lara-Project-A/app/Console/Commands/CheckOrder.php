<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Models\Order, App\User, App\Models\Log, App\Models\UserFirebaseToken;
use Carbon\Carbon;

class CheckOrder extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'check:order';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Check Order';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $currentDate = Carbon::now();
        $checkDateTime = Carbon::now()->addHours(2);

        $updateData = array('status' => 'order_cancelled', 'reason_for_cancellation' => 'Confirmation time expired');
        $orders = Order::where('status', 'order_received')
            /* ->where(function ($query) use($currentDate, $checkDateTime) {
                $query->where('created_at', '<', $currentDate)
                    ->where('created_at', '>=', $checkDateTime);
            }) */
            ->get(); 
        
        if ( $orders ) {
            foreach ($orders as $order) {
                
                $createdAtDate = Carbon::parse($order['created_at']);
                $currentEMI = $currentDate->diffInMinutes($createdAtDate);
                if ( $currentEMI >= 60 ) {
                    $update = Order::find($order->id)->update($updateData);
                    if ( $update ) {
                        
                        // log
                        $vendorAdmin = User::select('id')
                            ->where('vendor_id', $order['vendor_id'])
                            ->where('role', 'vendor_admin')
                            ->first();

                        $logData = array('common_id' => $order['id'], 'action_type' => 'order', 'action' => 'change_status', 'old' => $order['status'], 'new' => 'order_cancelled', 'description' => '', 'action_by' => $vendorAdmin['id'], 'user_ip' => '', 'is_read' => json_encode(array($order['customer_id'] => false)));
                        $logId = Log::create($logData);
                        // log

                        // Send Notification to Customer
                        $tokens = UserFirebaseToken::where('user_id', $order['customer_id'])->pluck('firebase_token')->toArray();
                        $notification = array(
                            'title' => ucwords(str_replace('_', ' ', 'order_cancelled')),
                            'body' => "#".$order['sequence_id']." order's status changed to ".str_replace('_', ' ', 'order_cancelled').".",
                            'data' => array(
                                'order_id' => $order['id'],
                                'mode' => 'order_status_change',
                                'notification_id' => $logId['id'],
                            )
                        );
                        $msg = '';
                        try {
                            $msg = app('App\Http\Controllers\Controller')->sendNotification($notification, $tokens);
                        } catch (\Throwable $th) {
                            
                        }
                    }
                }

            }
        } 
    }
}
