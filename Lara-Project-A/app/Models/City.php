<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class City extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'country_id', 'state_id', 'name'
    ];

    /**
     * Get the post that owns the comment.
     */
    public function country()
    {
        return $this->belongsTo('App\Models\Country');
    }

    /**
     * Get the post that owns the comment.
     */
    public function state()
    {
        return $this->belongsTo('App\Models\State');
    }
}
