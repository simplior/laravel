<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Payment extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'order_id', 'customer_id', 'vendor_id', 'log_id', 'transaction_sequence_id', 'payment_type', 'amount', 'payment_via', 'payment_details', 'status', 'settled_at'
    ];
}
