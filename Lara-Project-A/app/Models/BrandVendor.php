<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Relations\Pivot;

class BrandVendor extends Pivot
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'vendor_id', 'user_id'
    ];

}
