<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class VendorCarModelSetting extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'vendor_id', 'car_model_id', 'segment'
    ];
}
