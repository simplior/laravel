<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Storage;

class Service extends Model
{
    protected $appends = ['image_url'];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'parent_id', 'name', 'description', 'image', 'order_number', 'status', 'is_deleted'
    ];

    /**
     * Get the image full URL
     */
    public function getImageUrlAttribute()
    {
        $value = $this->image;
        if ( $value != '' && $value != null ) {
            return Storage::disk('public')->url('uploads/services/' . $value);
        } else {
            return asset('images/portrait/default.jpg');
        }
    }

    /**
     * Get the comments for the blog post.
     */
    public function vendors()
    {
        return $this->belongsToMany('App\Models\Vendor')
            ->withTimestamps()
            ->using('App\Models\ServiceVendor')
            ->orderBy('id', 'desc');
    }

    /**
     * Get the comments for the blog post.
     */
    public function subServices()
    {
        return $this->hasMany('App\Models\Service', 'parent_id')
            ->where('is_deleted', '0')
            ->orderBy('order_number', 'asc');
    }
    
    /**
     * Get the comments for the blog post.
     */
    public function subServicesActive()
    {
        return $this->hasMany('App\Models\Service', 'parent_id')
            ->where('status', '1')
            ->where('is_deleted', '0')
            ->orderBy('order_number', 'asc');
    }

    /**
     * Get the post that owns the comment.
     */
    public function parent()
    {
        return $this->belongsTo('App\Models\Service', 'id');
    }

    /**
     * Get the comments for the blog post.
     */
    public function segment_price()
    {
        if ( auth()->user()->role == 'admin' ) {
            return $this->hasOne('App\Models\VendorServiceSetting')
                ->where('vendor_id', 0)
                ->orderBy('id', 'desc');
        } else {
            return $this->hasOne('App\Models\VendorServiceSetting')
                ->where('vendor_id', auth()->user()->vendor_id)
                ->orderBy('id', 'desc');
        }
    }

    /**
     * Get the comments for the blog post.
     */
    public function default_segment_price()
    {
        return $this->hasOne('App\Models\VendorServiceSetting')
            ->where('vendor_id', '0')
            ->orderBy('id', 'desc');
    }
}
