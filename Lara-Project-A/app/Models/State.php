<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class State extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'country_id', 'name'
    ];

    /**
     * Get the post that owns the comment.
     */
    public function country()
    {
        return $this->belongsTo('App\Models\Country');
    }
}
