<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UserCar extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id', 'car_model_id', 'car_number', 'fuel_type', 'gear_type', 'manufacturing_year', 'total_travelled', 'avg_monthly_usage', 'is_deleted'
    ];

    /**
     * Get the post that owns the comment.
     */
    public function user()
    {
        return $this->belongsTo('App\User');
    }

    /**
     * Get the post that owns the comment.
     */
    public function car_model()
    {
        return $this->belongsTo('App\Models\CarModel');
    }
}
