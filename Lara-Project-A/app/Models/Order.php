<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class Order extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'temp_order_id', 'customer_id', 'vendor_id', 'user_car_id', 'user_address_id', 'sequence_id', 'pickup_type', 'pickup_person_id', 'sub_total', 'discount', 'tax', 'total', 'promo_code', 'service_date', 'service_time_slot', 'status', 'reason_for_cancellation', 'additional_comments', 'is_awaiting_for_approval', 'payment_type', 'payment_status', 'is_deleted'
    ];

    public function getServiceDateAttribute($value)
    {
        return Carbon::parse($value);
    }

    public function getCreatedAtAttribute($value)
    {
        return Carbon::parse($value);
    }

    /**
     * Get the comments for the blog post.
     */
    public function items()
    {
        return $this->hasMany('App\Models\OrderItem')
            ->where('status', '!=', 'cancelled')
            ->orderBy('parent_service_id', 'desc');
    }

    /**
     * Get the comments for the blog post.
     */
    public function before_service_documents()
    {
        return $this->hasMany('App\Models\OrderDocument')
            ->where('type', 'before_service')
            ->orderBy('id', 'desc');
    }

    /**
     * Get the comments for the blog post.
     */
    public function after_service_documents()
    {
        return $this->hasMany('App\Models\OrderDocument')
            ->where('type', 'after_service')
            ->orderBy('id', 'desc');
    }

    /**
     * Get the post that owns the comment.
     */
    public function customer()
    {
        return $this->belongsTo('App\User', 'customer_id');
    }

    /**
     * Get the post that owns the comment.
     */
    public function pickup_person()
    {
        return $this->belongsTo('App\User', 'pickup_person_id');
    }

    /**
     * Get the post that owns the comment.
     */
    public function vendor()
    {
        return $this->belongsTo('App\Models\Vendor');
    }

    /**
     * Get the post that owns the comment.
     */
    public function customer_car()
    {
        return $this->belongsTo('App\Models\UserCar', 'user_car_id');
    }

    /**
     * Get the post that owns the comment.
     */
    public function customer_address()
    {
        return $this->belongsTo('App\Models\UserAddress', 'user_address_id');
    }

    /**
     * Get the comments for the blog post.
     */
    public function payments()
    {
        return $this->hasOne('App\Models\Payment')
            ->select('id', 'order_id', 'transaction_sequence_id', 'payment_type', 'amount', 'payment_via', 'status');
    }

    /**
     * Get the comments for the blog post.
     */
    public function amount_credited()
    {
        return $this->hasMany('App\Models\Payment')
            ->where('payment_type', 'credited');
    }
}
