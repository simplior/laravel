<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Storage;

class CarModel extends Model
{
    protected $appends = ['image_url'];
    
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'brand_id', 'name', 'image', 'status', 'is_deleted'
    ];

    /**
     * Get the image full URL
     */
    public function getImageUrlAttribute()
    {
        $value = $this->image;
        if ( $value != '' && $value != null ) {
            return Storage::disk('public')->url('uploads/models/' . $value);
        } else {
            return asset('images/portrait/default.jpg');
        }
    }

    /**
     * Get the post that owns the comment.
     */
    public function brand()
    {
        return $this->belongsTo('App\Models\Brand');
    }

    /**
     * Get the comments for the blog post.
     */
    public function segment()
    {
        if ( auth()->user()->role == 'admin' ) {
            return $this->hasOne('App\Models\VendorCarModelSetting')
                ->where('vendor_id', 0)
                ->orderBy('id', 'desc');
        } else {
            return $this->hasOne('App\Models\VendorCarModelSetting')
                ->where('vendor_id', auth()->user()->vendor_id)
                ->orderBy('id', 'desc');
        }
    }

    /**
     * Get the comments for the blog post.
     */
    public function default_segment()
    {
        return $this->hasOne('App\Models\VendorCarModelSetting')
            ->where('vendor_id', '0')
            ->orderBy('id', 'desc');
    }
}
