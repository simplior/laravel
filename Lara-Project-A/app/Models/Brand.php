<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Storage;

class Brand extends Model
{
    protected $appends = ['image_url'];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'image', 'status', 'is_deleted'
    ];

    /**
     * Get the comments for the blog post.
     */
    public function models()
    {
        return $this->hasMany('App\Models\CarModel')
            ->where('is_deleted', '0')
            ->orderBy('id', 'desc');
    }

    /**
     * Get the comments for the blog post.
     */
    public function vendors()
    {
        return $this->belongsToMany('App\Models\Vendor')
            ->withTimestamps()
            ->using('App\Models\BrandVendor')
            ->orderBy('id', 'desc');
    }

    /**
     * Get the image full URL
     */
    public function getImageUrlAttribute()
    {
        $value = $this->image;
        if ( $value != '' && $value != null ) {
            return Storage::disk('public')->url('uploads/brands/' . $value);
        } else {
            return asset('images/portrait/default.jpg');
        }
    }
}
