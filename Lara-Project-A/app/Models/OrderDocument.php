<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Storage;

class OrderDocument extends Model
{
    protected $appends = ['document_url'];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'order_id', 'type', 'document', 'is_deleted'
    ];

    /**
     * Get the post that owns the comment.
     */
    public function order()
    {
        return $this->belongsTo('App\Models\Order');
    }

    /**
     * Get the image full URL
     */
    public function getDocumentUrlAttribute()
    {
        $value = $this->document;
        if ( $value != '' && $value != null ) {
            return Storage::disk('public')->url('uploads/orders/' . $this->order_id . '/' . $this->type . '/' . $value);
        } else {
            return asset('images/portrait/default.jpg');
        }
    }
}
