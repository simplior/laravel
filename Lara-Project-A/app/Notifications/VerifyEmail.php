<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;

class VerifyEmail extends Notification
{
    use Queueable;

    protected $data;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($data)
    {
        $this->data = $data;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        $this->data = array_merge($this->data, array(
            'base_url' => '',
            'providername' => 'CarsSeva',
            //'provideraddress' => 'CarsSeva',
            'providerlogo' => asset('images/logo/carsseva-logo.png'),
        ));
        $this->data['emailsubject'] = 'Email Verification';
        
        // write view file
        $file = fopen( resource_path( 'views/emails/temp1.blade.php'), 'w' );

        $headerData = file_get_contents(resource_path('views/emails/header.blade.php'));
        fwrite($file, $headerData);

        $templateData = file_get_contents(resource_path('views/emails/verify_email.blade.php'));
        fwrite($file, $templateData);

        $footerData = file_get_contents(resource_path('views/emails/footer.blade.php'));
        fwrite($file, $footerData);

        fclose($file);

        return (new MailMessage)
            ->subject($this->data['emailsubject'])
            ->view(
                'emails.temp1', $this->data
            );
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
