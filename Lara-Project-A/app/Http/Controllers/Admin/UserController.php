<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;

use App\User, App\Models\Vendor;
use Session, Redirect, Carbon\Carbon, Auth, Validator, Config, Hash, LaravelMsg91;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $currentUser = Auth::user();

        $users = User::where('is_deleted', '0');
        if ( isset($currentUser['role']) && $currentUser['role'] == 'vendor_admin' ) {
            $users = $users->where('vendor_id', $currentUser['vendor_id'])
                ->whereIn('role', array('vendor_admin', 'pickup_person'));
        } else {
            $users = $users->whereIn('role', array('vendor_admin', 'pickup_person'));
        }
        $users = $users->orderBy('id', 'desc')
            ->get();

        $vendors = Vendor::select('id', 'name')
            ->where('status', '1')
            ->where('is_deleted', '0')
            ->orderBy('id', 'desc')
            ->get();

        $roles = Config::get('custom.carsseva.roles');
        $breadcrumbs = [
            ['link' => "dashboard", 'name' => "Home"],
            ['name' => "Users"]
        ];
        
        return view('pages.user.index', [
            'breadcrumbs' => $breadcrumbs,
            'users' => $users,
            'roles' => $roles,
            'vendors' => $vendors,
            'currentUser' => $currentUser
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->all();
        $currentUser = Auth::user();
        if ( isset($currentUser['role']) && $currentUser['role'] == "vendor_admin") {
            $data['vendor_id'] = $currentUser['vendor_id'];
        }

        $validator = Validator::make($data, array(
            'role' => 'required',
            'vendor_id' => 'required',
            'name' => 'required',
            'contact_no' => [ 'required',
                Rule::unique('users', 'contact_no')->where(function ($query) {
                    return $query->whereIn('role', array('admin', 'vendor_admin','manager','pickup_person'))
                        ->where('is_deleted', 0);
                })
            ],
            'password' => ['required', 'confirmed']
        ));

        // process the login
        if ($validator->fails()) {
            return Redirect::back()
                ->withErrors($validator)
                ->withInput();
        }

        $data['password'] = Hash::make($data['password']);
        $user = User::create($data);
        if ( $user ) {

            // redirect
            Session::flash('success', 'User '.$data['name'].' successfully created!');
            return Redirect::to('users');
        } else {
            // redirect
            Session::flash('danger', 'Some thing is wrong. Please try again');
            return Redirect::to('users');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $currentUser = Auth::user();
        if ( $currentUser['id'] == $id ) {

            $user = User::find($id);
            
            $breadcrumbs = [
                ['link' => "dashboard", 'name' => "Home"],
                ['name' => "My Profile"]
            ];
            
            return view('pages.user.edit', [
                'breadcrumbs' => $breadcrumbs,
                'user' => $user
            ]);

        } else {
            return response( 'Permissions insuffisantes !', 403 );
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $data = $request->all();
        $currentUser = Auth::user();
        if ( isset($currentUser['role']) && $currentUser['role'] != "admin") {
            $data['vendor_id'] = $currentUser['vendor_id'];
        }
        
        $validator = Validator::make($data, array(
            'role' => 'required',
            'vendor_id' => 'required_unless:role,admin',
            'name' => 'required',
            'contact_no' => [ 'required',
                Rule::unique('users', 'contact_no')->where(function ($query) use($id) {
                    return $query->where('id', '!=', $id)
                        ->whereIn('role', array('admin', 'vendor_admin','manager','pickup_person'))
                        ->where('is_deleted', 0);
                })
            ]
        ));

        // process the login
        if ($validator->fails()) {
            
            /* return Redirect::back()
                ->withErrors($validator)
                ->withInput(); */
            
            $validatorString = implode(", ", $validator->messages()->all());
            Session::flash('danger', $validatorString);
            return Redirect::back();
        }

        unset($data['password']);
        $user = User::find($id);
        if ( $user->update($data) ) {

            // redirect
            Session::flash('success', 'User '.$data['name'].' successfully updated!');
        } else {
            // redirect
            Session::flash('danger', 'Some thing is wrong. Please try again');
        }
        
        if ( isset($data['call_from']) && $data['call_from'] == 'profile' ) {
            return Redirect::to('users/'.$id.'/edit');
        } else {
            return Redirect::to('users');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $currentUser = Auth::user();
        $data['is_deleted'] = '1';

        $itme = User::find($id);
        if ( $itme->update($data) ) {

            // redirect
            Session::flash('success', 'User successfully deleted!');
            return Redirect::to('users');
        } else {

            // redirect
            Session::flash('danger', 'Some thing is wrong. Please try again');
            return Redirect::to('users');
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function bulkAction(Request $request)
    {
        $data = $request->all();
        $currentUser = Auth::user();
        $validator = Validator::make($data, array(
            'action' => 'required',
            'ids' => 'required',
        ));

        // process the login
        if ($validator->fails()) {

            $response = array(
                'status' => 500,
                'message' => 'Invalid data',
                'data' => $validator->errors()
            );
            return response()->json($response, $response['status']);
        }

        $dataUpdate = array();
        if ( isset($data['action']) && $data['action'] == 'active' ) {
            $dataUpdate['status'] = '1';
        } else if ( isset($data['action']) && $data['action'] == 'inactive' ) {
            $dataUpdate['status'] = '0';
        } else if ( isset($data['action']) && $data['action'] == 'delete' ) {
            $dataUpdate['is_deleted'] = '1';
        }

        $item = User::find($data['ids']);
        if ( $item->update($dataUpdate) ) {
            
            Session::flash('success', 'All selected items successfully '.$data['action'].'d!');
            $response = array(
                'status' => 200,
                'message' => ''
            );
        } else {
            
            Session::flash('danger', 'Some thing is wrong. Please try again');
            $response = array(
                'status' => 500,
                'message' => ''
            );
        }

        return response()->json($response, $response['status']);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function changePassword(Request $request)
    {
        $data = $request->all();
        $currentUser = Auth::user();
        
        $validator = Validator::make($data, array(
            'user_id' => 'required',
            'password' => ['required', 'confirmed']
        ));

        // process the login
        if ($validator->fails()) {
            return Redirect::back()
                ->withErrors($validator)
                ->withInput();
        }
        
        $user = User::find($data['user_id']);
        $newPassword = $data['password'];
        $data['password'] = Hash::make($data['password']);
        if ( $user->update($data) ) {


            // Send Welcome SMS with password
            $msg = "Hello ".$user['name'].", your CarsSeva account password has been changed.\nYour new password is ".$newPassword;
            try {
                $result = LaravelMsg91::message('91'.$user['contact_no'], $msg);
            } catch (\Throwable $th) {
                $result = '';
            }

            // redirect
            Session::flash('success', 'User '.$user['name'].' password successfully changed!');
        } else {
            // redirect
            Session::flash('danger', 'Some thing is wrong. Please try again');
        }

        if ( isset($data['call_from']) && $data['call_from'] == 'profile' ) {
            return Redirect::to('users/'.$data['user_id'].'/edit');
        } else {
            return Redirect::to('users');
        }
    }
}
