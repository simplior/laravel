<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;

use App\User, App\Models\UserFirebaseToken, App\Models\Service, App\Models\Promotion, App\Models\UserAddress, App\Models\Order, App\Models\TempOrder, App\Models\VerifyUserEmail;
use Validator, Auth, Config, Hash, Carbon\Carbon, File;
use App\Notifications\VerifyEmail;

class UserController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function checkNumber(Request $request)
    {
        $_mode = $request->header('Accept-Mode');
        $data = $request->all();
        $validator = Validator::make($data, array(
            'contact_no' => 'required',
        ));

        // process the login
        if ($validator->fails()) {
            
            $validatorString = implode(", ", $validator->messages()->all());
            $validatorArray = array_combine(
                array_map(function($key){ return $key.'_error'; }, array_keys($validator->errors()->toArray())),
                $validator->errors()->toArray()
            );

            $response = array(
                'response_code' => 400,
                'response_message' => $validatorString,
                'response_data' => $validatorArray
            );
            return response()->json($response, 200);
        }

        if ( $_mode == 'vendor' ) {
            $role = 'vendor_admin';
        } else {
            $role = 'customer';
        }

        $checkCustomer = User::where('role', $role)->where('contact_no', $data['contact_no'])->first();
        if( $checkCustomer ){ 

            $response = array(
                'response_code' => 200,
                'response_message' => ''
            );
        } else {
            $response = array(
                'response_code' => 500,
                'response_message' => "This ".$_mode." is not exist in portal"
            );
        }

        return response()->json($response, 200);
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function register(Request $request)
    {
        $data = $request->all();
        $_mode = $request->header('Accept-Mode');
        if ( $_mode == 'customer' ) {
            $checkUserWithRole = array('customer');
        } else {
            $checkUserWithRole = array('admin','vendor_admin','manager','pickup_person');
        }
        $validator = Validator::make($data, array(
            'name' => 'required',
            //'contact_no' => 'required|unique:users',
            'contact_no' => [ 'required',
                Rule::unique('users', 'contact_no')->where(function ($query) use($checkUserWithRole) {
                    return $query->whereIn('role', $checkUserWithRole)
                        ->where('is_deleted', 0);
                })
            ],
            'firebase_token' => 'required',
            //'registered_via' => 'required',
        ));

        // process the login
        if ($validator->fails()) {

            $validatorString = implode(", ", $validator->messages()->all());
            $validatorArray = array_combine(
                array_map(function($key){ return $key.'_error'; }, array_keys($validator->errors()->toArray())),
                $validator->errors()->toArray()
            );

            $response = array(
                'response_code' => 400,
                'response_message' => $validatorString,
                'response_data' => $validatorArray
            );
            return response()->json($response, 200);
        }

        // find seq id
        $prefix = Config::get('custom.carsseva.prefix.customer');
        $lastCustomer = User::select('sequence_id')->where('role', 'customer')->orderBy('ID', 'desc')->first();
        if ( isset($lastCustomer->sequence_id) ) {
            $data['sequence_id'] = $this->increaseSeqNo($lastCustomer->sequence_id, $prefix);
        } else {
            $data['sequence_id'] = $prefix.'0001';
        }

        $data['role'] = 'customer';
        $data['password'] = Hash::make('123123');

        $customer = User::create($data);
        if ( $customer ) {

            Auth::login($customer);
            $user = Auth::user();
            $token =  $user->createToken('CarsSeva')->accessToken;

            UserFirebaseToken::create(array(
                'user_id' => $user['id'],
                'oauth_token' => $token,
                'firebase_token' => $data['firebase_token'],
            ));

            $response = array(
                'response_code' => 200,
                'response_message' => 'Welcome to CarsSeva',
                'response_data' => array(
                    'id' => $user['id'],
                    'name' => $user['name'],
                    'contact_no' => $user['contact_no'],
                    'profile_pic_url' => $user['profile_pic_url'],
                    'token' => $token,
                )
            );

        } else {
            $response = array(
                'response_code' => 500,
                'response_message' => "Opps, Something went wrong please try again later!"
            );
        }

        return response()->json($response, 200);
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function login(Request $request)
    {
        $data = $request->all();
        $validator = Validator::make($data, array(
            'registered_via' => 'required',
            'contact_no' => 'required_if:registered_via,application',
            'firebase_token' => 'required',
            'name' => 'required_unless:registered_via,application',
            'email' => 'required_unless:registered_via,application',
            'is_email_verified' => 'required_unless:registered_via,application',
            'profile_pic' => 'required_unless:registered_via,application',
            'uid' => 'required_unless:registered_via,application',
        ));

        // process the login
        if ($validator->fails()) {

            $validatorString = implode(", ", $validator->messages()->all());
            $validatorArray = array_combine(
                array_map(function($key){ return $key.'_error'; }, array_keys($validator->errors()->toArray())),
                $validator->errors()->toArray()
            );

            $response = array(
                'response_code' => 400,
                'response_message' => $validatorString,
                'response_data' => $validatorArray
            );
            return response()->json($response, 200);
        }
        
        if ( isset($data['registered_via']) && $data['registered_via'] != 'application' ) {
            $checkEmailUser = User::where('email', $data['email'])->where('role', 'customer')->first();
            if ( $checkEmailUser ) {
                
                $data['social_data'] = serialize(array(
                    'uid' => $data['uid']
                ));

                if ( $checkEmailUser->update($data) ) {
                    Auth::login($checkEmailUser);
                    $attempt = true;
                }
            } else {
                
                // find seq id
                $prefix = Config::get('custom.carsseva.prefix.customer');
                $lastCustomer = User::select('sequence_id')->where('role', 'customer')->orderBy('ID', 'desc')->first();
                if ( isset($lastCustomer->sequence_id) ) {
                    $data['sequence_id'] = $this->increaseSeqNo($lastCustomer->sequence_id, $prefix);
                } else {
                    $data['sequence_id'] = $prefix.'0001';
                }

                $data['role'] = 'customer';
                $data['is_contact_no_verified'] = 0;
                $data['password'] = Hash::make('123123');

                $newuser = User::create($data);
                if ( $newuser ) {
                    Auth::login($newuser);
                    $attempt = true;
                }
            }
        } else {
            $attempt = Auth::attempt(['role' => 'customer', 'contact_no' => $data['contact_no'], 'password' => '123123']);
        }

        if( $attempt ){ 

            $user = Auth::user();
            $token =  $user->createToken('CarsSeva')->accessToken;
            
            UserFirebaseToken::create(array(
                'user_id' => $user['id'],
                'oauth_token' => $token,
                'firebase_token' => $data['firebase_token'],
            ));

            // Get Main Services
            $mainServices = Service::select('id', 'parent_id', 'name', 'description', 'image')
                ->where('parent_id', '0')
                ->where('status', '1')
                ->where('is_deleted', '0')
                ->orderBy('id', 'desc')
                ->get();

            // Get Promotions
            $promotions = Promotion::select('id', 'name', 'code', 'description', 'sign_off_type', 'amount')
                ->whereDate('start_date', '<=', Carbon::now())
                ->whereDate('end_date', '>', Carbon::now())
                ->where('status', '1')
                ->where('is_deleted', '0')
                ->orderBy('id', 'desc')
                ->get();

            $cars = array();
            if ( count($user->cars) > 0 ) {
                foreach ($user->cars as $k => $car) {
                    $cars[$k] = array(
                        'id' => $car['id'],
                        'car_model_id' => $car['car_model_id'],
                        'car_number' => $car['car_number'],
                        'car_model' => array(
                            'id' => $car->car_model->id,
                            'brand_id' => $car->car_model->brand_id,
                            'name' => $car->car_model->name,
                            'image_url' => $car->car_model->image_url,
                            'brand' => array(
                                'id' => $car->car_model->brand->id,
                                'name' => $car->car_model->brand->name,
                                'image_url' => $car->car_model->brand->image_url,
                            )
                        )
                    );
                }
            }

            // Recomm. Services
            $recommendation = TempOrder::select('id', 'order_id', 'customer_id')
                ->where('customer_id', $user->id)
                ->where('is_awaiting_for_approval', '1')
                ->with(array('items' => function($query) {
                    $query->select('id', 'temp_order_id', 'parent_service_id', 'service_id', 'service_name')
                        ->with('parent_service:id,name');
                }))
                ->orderBy('id', 'desc')
                ->get();

            $response = array(
                'response_code' => 200,
                'response_message' => 'Welcome to CarsSeva',
                'response_data' => array(
                    'user' => array(
                        'id' => $user['id'],
                        'name' => $user['name'],
                        'contact_no' => $user['contact_no'],
                        'profile_pic_url' => $user['profile_pic_url'],
                        'token' => $token,
                    ),
                    'cars' => $cars,
                    'recommendation' => $recommendation,
                    'main_services' => $mainServices,
                    'promotions' => $promotions,
                )
            );
        } else {
            $response = array(
                'response_code' => 401,
                'response_message' => "You've entered an invalid credential"
            );
        }

        return response()->json($response, 200);
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function loginVendor(Request $request)
    {
        $data = $request->all();
        $validator = Validator::make($data, array(
            'contact_no' => 'required',
            'password' => 'required',
            'firebase_token' => 'required',
        ));

        // process the login
        if ($validator->fails()) {

            $validatorString = implode(", ", $validator->messages()->all());
            $validatorArray = array_combine(
                array_map(function($key){ return $key.'_error'; }, array_keys($validator->errors()->toArray())),
                $validator->errors()->toArray()
            );

            $response = array(
                'response_code' => 400,
                'response_message' => $validatorString,
                'response_data' => $validatorArray
            );
            return response()->json($response, 200);
        }

        if( Auth::attempt(['role' => 'vendor_admin', 'contact_no' => $data['contact_no'], 'password' => $data['password'], 'is_deleted' => 0, 'status' => 1]) ){ 

            $user = Auth::user();
            $token =  $user->createToken('CarsSeva')->accessToken;

            UserFirebaseToken::create(array(
                'user_id' => $user['id'],
                'oauth_token' => $token,
                'firebase_token' => $data['firebase_token'],
            ));

            // Get Promotions
            $promotions = Promotion::select('id', 'name', 'code', 'description', 'sign_off_type', 'amount')
                ->whereDate('start_date', '<=', Carbon::now())
                ->whereDate('end_date', '>', Carbon::now())
                ->whereRaw('(JSON_CONTAINS(vendors, "true", "$.'.$user['vendor_id'].'") OR JSON_CONTAINS(vendors, "true", "$.all"))')
                ->where('status', '1')
                ->where('is_deleted', '0')
                ->orderBy('id', 'desc')
                ->get();

            // Order Count Based on Status
            $order_count = array(
                'order_received' => 0,
                'order_confirmed' => 0,
                'order_cancelled' => 0,
                'team_enroute' => 0,
                'car_picked_up' => 0,
                'service_started' => 0,
                'service_completed' => 0,
                'delivered' => 0,
            );
            $orders = Order::selectRaw('COUNT(id) as count, status')
                ->where('vendor_id', $user['vendor_id'])
                ->groupBy('status')
                ->get();

            if ( $orders ) {
                foreach ($orders as $order) {
                    $order_count[$order['status']] = $order['count'];
                }
            }

            // List of pending orders
            $pendingOrders = Order::where('vendor_id', $user['vendor_id'])
                ->where('status', 'order_received')
                ->pluck('sequence_id');

            $response = array(
                'response_code' => 200,
                'response_message' => 'Welcome to CarsSeva',
                'response_data' => array(
                    'user' => array(
                        'id' => $user['id'],
                        'vendor_id' => $user['vendor_id'],
                        'name' => $user['name'],
                        'contact_no' => $user['contact_no'],
                        'profile_pic_url' => $user['profile_pic_url'],
                        'token' => $token,
                        'vendor' => array(
                            'id' => $user->vendor->id,
                            'name' => $user->vendor->name,
                            'logo_url' => $user->vendor->logo_url,
                        ),
                    ),
                    'pending_orders' => array(
                        'count' => $order_count['order_received'],
                        'orders' => $pendingOrders
                    ),
                    'order_count' => $order_count,
                    'promotions' => $promotions,
                    'total_money' => 10000,
                    'pending_money' => 1000,
                )
            );
        } else {
            $response = array(
                'response_code' => 401,
                'response_message' => "You've entered an invalid credential"
            );
        }

        return response()->json($response, 200);
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function resetPassword(Request $request)
    {
        $data = $request->all();
        $validator = Validator::make($data, array(
            'contact_no' => 'required',
            'password' => 'required|confirmed'
        ));

        // process the login
        if ($validator->fails()) {

            $validatorString = implode(", ", $validator->messages()->all());
            $validatorArray = array_combine(
                array_map(function($key){ return $key.'_error'; }, array_keys($validator->errors()->toArray())),
                $validator->errors()->toArray()
            );

            $response = array(
                'response_code' => 400,
                'response_message' => $validatorString,
                'response_data' => $validatorArray
            );
            return response()->json($response, 200);
        }
        
        $checkUser = User::where('role', 'vendor_admin')->where('contact_no', $data['contact_no'])->first();
        if( $checkUser ){ 

            $updateData = array(
                'password' => Hash::make($data['password'])
            );
            if ( $checkUser->update($updateData) ) {

                $response = array(
                    'response_code' => 200,
                    'response_message' => 'Password Changed!'
                );
            } else {

                $response = array(
                    'response_code' => 500,
                    'response_message' => "Opps, Something went wrong please try again later!"
                );
            }
        } else {
            $response = array(
                'response_code' => 500,
                'response_message' => "This vendor is not exist in portal"
            );
        }

        return response()->json($response, 200);
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function logout(Request $request)
    {
        $currentUser = Auth::user();
        if( $currentUser->token()->revoke() ){ 

            $oauth_token = str_replace('Bearer ', '', $request->header('Authorization'));
            UserFirebaseToken::where('oauth_token', $oauth_token)->delete();

            $response = array(
                'response_code' => 200,
                'response_message' => 'logged Out',
            );
        } else {
            $response = array(
                'response_code' => 500,
                'response_message' => "Opps, Something went wrong please try again later!"
            );
        }

        return response()->json($response, 200);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function profile(Request $request)
    {
        $_mode = $request->header('Accept-Mode');
        $currentUser = Auth::user();

        $user = User::select('id', 'vendor_id', 'sequence_id', 'name', 'email', 'contact_no', 'profile_pic', 'is_name_verified', 'is_contact_no_verified', 'is_email_verified', 'registered_via');

        if ( $_mode == 'customer' ) {
            $user = $user->with(array('addresses' => function($query){
                $query->select('id', 'user_id', 'friendly_name', 'address_type', 'address_line_1', 'address_line_2', 'state_id', 'city_id', 'latitude', 'longitude', 'zipcode', 'is_primary')
                    ->with(array('state' => function($query){
                        $query->select('id', 'country_id', 'name')->with('country:id,name');
                    }))
                    ->with('city:id,name');
            }));
        } else {
            $user = $user->with(array('vendor' => function($query){
                $query->select('id', 'name', 'contact_no', 'address_line_1', 'address_line_2', 'state_id', 'city_id', 'latitude', 'longitude', 'zipcode', 'logo')
                    ->with(array('state' => function($query){
                        $query->select('id', 'country_id', 'name')->with('country:id,name');
                    }))
                    ->with('city:id,name');
            }));
        }

        $user = $user->where('status', '1')
            ->where('is_deleted', '0')
            ->find($currentUser->id)->toArray();
        
        unset($user['aadhar_card_url']);
        unset($user['PAN_card_url']);
        
        $response = array(
            'response_code' => 200,
            'response_message' => '',
            'response_data' => $user
        );

        return response()->json($response, 200);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $currentUser = Auth::user();
        $data = $request->all();
        $_mode = $request->header('Accept-Mode');

        if ( $_mode == 'customer' ) {
            $checkUserWithRole = array('customer');
        } else {
            $checkUserWithRole = array('admin','vendor_admin','manager','pickup_person');
        }

        $valArr = array(
            'registered_via' => 'required',
            'name' => 'required',
            'email' => 'required_unless:registered_via,application',
        );
        
        if ( isset($data['registered_via']) && $data['registered_via'] != '' ) {
            if ( $data['registered_via'] == 'application' ) {
                $valArr['contact_no'] = ['required',
                    Rule::unique('users', 'contact_no')->where(function ($query) use($checkUserWithRole, $currentUser) {
                        return $query->where('id', '!=', $currentUser['id'])
                            ->whereIn('role', $checkUserWithRole)
                            ->where('is_deleted', 0);
                    })
                ];
            }
        }

        $validator = Validator::make($data, $valArr);

        // process the login
        if ($validator->fails()) {

            $validatorString = implode(", ", $validator->messages()->all());
            $validatorArray = array_combine(
                array_map(function($key){ return $key.'_error'; }, array_keys($validator->errors()->toArray())),
                $validator->errors()->toArray()
            );

            $response = array(
                'response_code' => 400,
                'response_message' => $validatorString,
                'response_data' => $validatorArray
            );
            return response()->json($response, 200);
        }

        if( $currentUser->update($data) ){ 

            if ( isset($data['primary_address_id']) && $data['primary_address_id'] != '' ) {
                UserAddress::where('user_id', $currentUser->id)->update(array('is_primary' => 0));
                UserAddress::find($data['primary_address_id'])->update(array('is_primary' => 1));
            }

            if ( isset($data['profile_pic']) ) {
                $profile_pic = $data['profile_pic'];
                $subpath = "public/uploads/users/";
                $fileStoreName = Carbon::now()->timestamp .'.' . $profile_pic->getClientOriginalExtension();
                $path = storage_path('app') . "/" . $subpath;
                if( !File::isDirectory($path) ){
                    File::makeDirectory($path, 0755, true, true);
                }
                
                if ( $profile_pic->move($path, $fileStoreName) ) {
                    User::find($currentUser->id)->update(array('profile_pic' => $fileStoreName));
                }
            }

            $user = User::select('id', 'sequence_id', 'name', 'email', 'contact_no', 'profile_pic', 'is_name_verified', 'is_contact_no_verified', 'is_email_verified')
                ->with('addresses:id,user_id,address_line_1,address_line_2,state,city,latitude,longitude,zipcode,is_primary')
                ->with(array('addresses' => function($query){
                    $query->select('id', 'user_id', 'address_line_1', 'address_line_2', 'state_id', 'city_id', 'latitude', 'longitude', 'zipcode', 'is_primary')
                        ->with(array('state' => function($query){
                            $query->select('id', 'country_id', 'name')->with('country:id,name');
                        }))
                        ->with('city:id,name');
                }))
                ->where('status', '1')
                ->where('is_deleted', '0')
                ->find($currentUser->id)->toArray();
            
            unset($user['aadhar_card_url']);
            unset($user['PAN_card_url']);

            $response = array(
                'response_code' => 200,
                'response_message' => 'Updated!',
                'response_data' => $user
            );
        } else {
            $response = array(
                'response_code' => 500,
                'response_message' => "Opps, Something went wrong please try again later!"
            );
        }

        return response()->json($response, 200);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function verifyEmail(Request $request)
    {
        $data = $request->all();
        $currentUser = Auth::user();
        $validator = Validator::make($data, array(
            'email_id' => 'required|unique:users,email,'.$currentUser->id
        ));

        // process the login
        if ($validator->fails()) {

            $validatorString = implode(", ", $validator->messages()->all());
            $validatorArray = array_combine(
                array_map(function($key){ return $key.'_error'; }, array_keys($validator->errors()->toArray())),
                $validator->errors()->toArray()
            );

            $response = array(
                'response_code' => 400,
                'response_message' => $validatorString,
                'response_data' => $validatorArray
            );
            return response()->json($response, 200);
        }

        $data['user_id'] = $currentUser->id;
        $data['token'] = sha1(time());

        $verifyEmail = VerifyUserEmail::create($data);
        if ( $verifyEmail ) {
            
            $emailData = array(
                'name' => $currentUser['name'],
                'verification_link' => url( "/user/verify/" . $verifyEmail['token'] ),
            );
    
            //try {
                $currentUser->setEmail($verifyEmail['email_id']);
                $mail = $currentUser->notify(
                    new VerifyEmail($emailData)
                );
            //} catch (\Exception $e) {}
            
            User::find($currentUser->id)->update(array('email' => $verifyEmail['email_id'], 'is_email_verified' => '0'));

            $response = array(
                'response_code' => 200,
                'response_message' => 'Verification link sent to your email id, Please verify using link!'
            );

        } else {

            $response = array(
                'response_code' => 500,
                'response_message' => "Opps, Something went wrong please try again later!"
            );
        }

        return response()->json($response, 200);
    }
}
