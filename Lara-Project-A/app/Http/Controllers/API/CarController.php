<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;

use Validator, Carbon\Carbon;
use Auth, App\Models\Brand, App\Models\CarModel, App\Models\UserCar, App\Models\Order;

class CarController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $currentUser = Auth::user();
        
        $mycar = UserCar::select('id', 'car_model_id', 'car_number')
            ->with(array('car_model' => function($query){
                $query->select('id', 'brand_id', 'name', 'image')->with('brand:id,name');
            }))
            ->where('user_id', $currentUser->id)
            ->where('is_deleted', '0')
            ->orderBy('id', 'desc')
            ->get();

        $response = array(
            'response_code' => 200,
            'response_message' => '',
            'response_data' => $mycar
        );

        return response()->json($response, 200);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->all();
        $validator = Validator::make($data, array(
            'car_model_id' => 'required|integer',
            'fuel_type' => 'required',
            'gear_type' => 'required',
            'manufacturing_year' => 'required',
            'total_travelled' => 'required',
            'avg_monthly_usage' => 'required',
            'car_number' => 'required',
            /* 'car_number' => [ 'required',
                Rule::unique('user_cars', 'car_number')->where(function ($query) {
                    return $query->where('is_deleted', 0);
                })
            ], */
        ));

        // process the login
        if ($validator->fails()) {

            $validatorString = implode(", ", $validator->messages()->all());
            $validatorArray = array_combine(
                array_map(function($key){ return $key.'_error'; }, array_keys($validator->errors()->toArray())),
                $validator->errors()->toArray()
            );

            $response = array(
                'response_code' => 400,
                'response_message' => $validatorString,
                'response_data' => $validatorArray
            );
            return response()->json($response, 200);
        }

        $currentUser = Auth::user();
        
        $data['user_id'] = $currentUser->id;
        $data['manufacturing_year'] = Carbon::parse(str_replace('/', '-', $data['manufacturing_year']))->format('Y-m-d');

        $mycar = UserCar::create($data);
        if( $mycar ){ 

            $response = array(
                'response_code' => 200,
                'response_message' => 'Stored!'
            );
        } else {
            $response = array(
                'response_code' => 500,
                'response_message' => "Opps, Something went wrong please try again later!"
            );
        }

        return response()->json($response, 200);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $response_message = '';
        $mycar = UserCar::select('id', 'car_model_id', 'car_number', 'fuel_type', 'gear_type', 'manufacturing_year', 'total_travelled', 'avg_monthly_usage')
            ->where('is_deleted', 0)
            ->find($id);
        
        if ( $mycar ) {
            $mycar['manufacturing_year'] = Carbon::parse(str_replace('/', '-', $mycar['manufacturing_year']))->format('Y-m');
        } else {
            $response_message = 'Car not available in our portal.';
            $mycar = array();
        }

        $response = array(
            'response_code' => 200,
            'response_message' => $response_message,
            'response_data' => $mycar
        );

        return response()->json($response, 200);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $mycar = UserCar::where('is_deleted', 0)->find($id);
        if ( $mycar ) {

            $data = $request->all();
            $validator = Validator::make($data, array(
                'car_model_id' => 'required|integer',
                'fuel_type' => 'required',
                'gear_type' => 'required',
                'manufacturing_year' => 'required',
                'total_travelled' => 'required',
                'avg_monthly_usage' => 'required',
                'car_number' => 'required',
                /* 'car_number' => [ 'required',
                    Rule::unique('user_cars', 'car_number')->where(function ($query) use($id) {
                        return $query->where('id', '!=', $id)
                            ->where('is_deleted', 0);
                    })
                ], */
            ));

            // process the login
            if ($validator->fails()) {

                $validatorString = implode(", ", $validator->messages()->all());
                $validatorArray = array_combine(
                    array_map(function($key){ return $key.'_error'; }, array_keys($validator->errors()->toArray())),
                    $validator->errors()->toArray()
                );

                $response = array(
                    'response_code' => 400,
                    'response_message' => $validatorString,
                    'response_data' => $validatorArray
                );
                return response()->json($response, 200);
            }

            $currentUser = Auth::user();
            
            $data['user_id'] = $currentUser->id;
            $data['manufacturing_year'] = Carbon::parse(str_replace('/', '-', $data['manufacturing_year']))->format('Y-m-d');

            if( $mycar->update($data) ){ 

                $response = array(
                    'response_code' => 200,
                    'response_message' => 'Updated!'
                );
            } else {
                $response = array(
                    'response_code' => 500,
                    'response_message' => "Opps, Something went wrong please try again later!"
                );
            }
        } else {
            $response = array(
                'response_code' => 500,
                'response_message' => "Car not available in our portal."
            );
        }

        return response()->json($response, 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $data = array(
            'is_deleted' => 1
        );
        
        $checkOrder = Order::where('user_car_id', $id)
            ->whereNotIn('status', array('delivered', 'order_cancelled'))
            ->count();
        
        if ( $checkOrder <= 0 ) {
            $mycar = UserCar::find($id);
            if( $mycar->update($data) ){ 

                $response = array(
                    'response_code' => 200,
                    'response_message' => 'Deleted!'
                );
            } else {
                $response = array(
                    'response_code' => 500,
                    'response_message' => "Opps, Something went wrong please try again later!"
                );
            }
        } else {
            $response = array(
                'response_code' => 500,
                'response_message' => "You are not able to delete this car, because few orders are placed with this car!"
            );
        }

        return response()->json($response, 200);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function brands()
    {
        $currentUser = Auth::user();

        $brands = Brand::select('id', 'name', 'image')
            ->where('is_deleted', '0')
            ->where('status', '1')
            ->orderBy('id', 'asc')
            ->get();

        $response = array(
            'response_code' => 200,
            'response_message' => '',
            'response_data' => $brands
        );

        return response()->json($response, 200);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function models(Request $request)
    {
        $data = $request->all();
        $validator = Validator::make($data, array(
            'brand_id' => 'required'
        ));

        // process the login
        if ($validator->fails()) {

            $validatorString = implode(", ", $validator->messages()->all());
            $validatorArray = array_combine(
                array_map(function($key){ return $key.'_error'; }, array_keys($validator->errors()->toArray())),
                $validator->errors()->toArray()
            );

            $response = array(
                'response_code' => 400,
                'response_message' => $validatorString,
                'response_data' => $validatorArray
            );
            return response()->json($response, 200);
        }

        $currentUser = Auth::user();
        $models = CarModel::select('id', 'name', 'image')
            ->where('brand_id', $data['brand_id'])
            ->where('is_deleted', '0')
            ->where('status', '1')
            ->orderBy('id', 'asc')
            ->get(); 

        $response = array(
            'response_code' => 200,
            'response_message' => '',
            'response_data' => $models
        );

        return response()->json($response, 200);
    }
}
