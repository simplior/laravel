<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Client;

use Auth, Validator, Redirect, Session;
use App\User, App\Models\Vendor;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    // Login
    public function showLoginForm(){
        $pageConfigs = [
            'bodyClass' => "bg-full-screen-image",
            'blankPage' => true
        ];

        return view('/auth/login', [
            'pageConfigs' => $pageConfigs
        ]);
    }

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function login(Request $request)
    {
        $data = $request->all();
        $validator = Validator::make($data, array(
            'contact_no' => 'required',
            'password' => 'required'
        ));

        // process the login
        if ($validator->fails()) {
            return Redirect::back()
                ->withErrors($validator)
                ->withInput();
        }

        if( Auth::attempt(['contact_no' => $data['contact_no'], 'password' => $data['password'], 'role' => array('admin', 'vendor_admin'), 'is_deleted' => 0], true) ){ 

            $currentUser = Auth::user();
            if ( $currentUser['status'] != '0' && $currentUser['is_deleted'] == '0' ) {
                
                if ( $currentUser['role'] != 'admin' ) {
                    $vendorData = Vendor::select('id', 'status', 'account_section', 'is_deleted')->find($currentUser['vendor_id']);
                    if ( $vendorData['status'] != '0' ) {
                        
                        $account_section = unserialize($vendorData['account_section']);
                        //dd($currentUser, $account_section, in_array(false, $account_section));
                        if ( in_array(false, $account_section) ) {
                            return Redirect::to('vendor/'.$currentUser['vendor_id'].'/edit');
                        } else {
                            return Redirect::to('dashboard');
                        }

                    } else {
                        Auth::logout();
                        return Redirect::back()->with('danger', "Your vendor is not activated now!");
                    }
                } else {
                    return Redirect::to('dashboard');
                }

            } else {
                Auth::logout();
                return Redirect::back()->with('danger', "Your account is not activated now!");
            }
            
        } else {
            return Redirect::back()->with('danger', "You've entered an invalid credential!");
        }
    }

    /**
     * Log the user out of the application.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function logout(Request $request)
    {
        $this->guard()->logout();

        $request->session()->invalidate();

        return $this->loggedOut($request) ?: redirect('/login');
    }
}
