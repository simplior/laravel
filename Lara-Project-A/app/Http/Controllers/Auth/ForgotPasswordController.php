<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
//use Illuminate\Foundation\Auth\SendsPasswordResetEmails;

use Illuminate\Http\Request;
use Illuminate\Http\Response;

use App\User;
use Validator, View, Redirect, DB, Session, LaravelMsg91, Carbon\Carbon;

class ForgotPasswordController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Password Reset Controller
    |--------------------------------------------------------------------------
    |
    | This controller is responsible for handling password reset emails and
    | includes a trait which assists in sending these notifications from
    | your application to your users. Feel free to explore this trait.
    |
    */

    //use SendsPasswordResetEmails;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    public function showLinkRequestForm(){
      $pageConfigs = [
        'bodyClass' => "bg-full-screen-image",
        'blankPage' => true
      ];

      return view('/auth/passwords/email', [
        'pageConfigs' => $pageConfigs
      ]);
    }

    public function sendResetLinkEmail(Request $request)
    {
      $data = $request->all();
      $validator = Validator::make($data, array(
          'contact_no' => ['required']
      ));

      // process the login
      if ($validator->fails()) {
          return Redirect::back()
              ->withErrors($validator)
              ->withInput();
      }

      $user = User::where('contact_no', $data['contact_no'])
        ->whereIn('role', array('admin', 'vendor_admin'))
        ->where('is_deleted', 0)
        ->first();
      
      if ( $user ) {

        // remove token
        DB::table('password_resets')
          ->where('contact_no', '=', $data['contact_no'])
          ->delete();

        $token = str_random(64);
        //$token = app(\Illuminate\Auth\Passwords\PasswordBroker::class)->createToken($user);

        DB::table('password_resets')->insert([
          'contact_no' => $user->contact_no, 
          'token' => $token,
          'created_at' => Carbon::now(),
        ]);
        
        $link = url('/') . '/password/reset/' . $token . '?contact_no=' . urlencode($user->contact_no);
        $msg = "Hello ".$user['name']."\nYour are requested for reset password, Using reset password link you can reset you account password.\n\nReset Password: ".$link;
        try {
            $result = LaravelMsg91::message('91'.$data['contact_no'], $msg);
        } catch (\Throwable $th) {
            $result = '';
        }
        
        if ( $result != '' ) {
          return Redirect::back()->with('success', "We have e-mailed your password reset link!");
        } else {
          return Redirect::back()
            ->withInput()
            ->with('danger', "Opps, something went wrong please try again later!");
        }
        
      } else {
        return Redirect::back()
          ->withInput()
          ->with('danger', "Your are not registered in our portal!");
      }
    }
}
