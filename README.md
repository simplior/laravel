### Laravel Practices ###

* Invokable Controllers
* Unsigned Integer
* OrderBy on Eloquent relationships
* Raw DB Queries
* $loop variable in foreach
* Eloquent where date methods
* Route group within a group
* Increments and decrements
* Database migrations column types
* Soft-deletes: multiple restore
* Image validation
* Testing email into laravel.log
* Use Eloquent Orm
* Use Naming Conventions
* Using Migrations